////! import "../lib/_lib.j"

// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/_base.zn
//! zinc
library base requires lib {

    public constant player PLAYER_NPC = Player(2);
    public constant player PLAYER_ENEMY = Player(7);
    public constant player PLAYER_NEUTRAL = Player(PLAYER_NEUTRAL_PASSIVE);
    public constant integer MAX_PLAYERS = 10;
    public constant integer MIN_HANDLE_ID = 0x100000;

    public constant real FPS_50 = 0.02;
    public constant real FPS_30 = 0.03;
    public constant real FPS_25 = 0.04;
    
    public string COLOR_OF_PLAYER[];

    public type SmallIntList extends integer[16];
    public type IntList extends integer[32];
    public type LargeIntList extends integer[128];
    public type RealList extends real[32];
    public type LargeRealList extends real[128];
    public type SmallDestructableList extends destructable[16];
    public type DestructableList extends destructable[32];

    public type CallBackFunc extends function();
    public type CallBack extends function(integer);
    public type CallBackWithReturn extends function(integer) -> integer;
    public type CallBackFuncList extends CallBackFunc[32];
    public type CallBackList extends CallBack[32];
    public type CallBackWithReturnList extends CallBackWithReturn[32];

    private function initPlayerColor() {
        COLOR_OF_PLAYER[0]="|c00ff0000";//玩家1
        COLOR_OF_PLAYER[1]="|c000080FF";//玩家2
        COLOR_OF_PLAYER[2]="|c0018e7bd";//玩家3
        COLOR_OF_PLAYER[3]="|c008000FF";//玩家4
        COLOR_OF_PLAYER[4]="|c00ffff00";//玩家5
        COLOR_OF_PLAYER[5]="|c00ff8a08";//玩家6
        COLOR_OF_PLAYER[6]="|cff00ff00";//玩家7
        COLOR_OF_PLAYER[7]="|cffff00ff";//玩家8
        COLOR_OF_PLAYER[8]="|cff808080";//玩家9
        COLOR_OF_PLAYER[9]="";
        COLOR_OF_PLAYER[10]="";
        COLOR_OF_PLAYER[11]="";
    }

    
    private DataCache g_p_ht_type;
    private DataCache g_p_ht;
    
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Integer.zinc
public struct Integer {
    public integer m_negative = 1;
    public integer m_value = 0;
    public integer m_length = 0;
    public integer m_arrLength = 0;
    //单个数组元素含有数字的个数
    public integer m_arrElementSize = 0;
    private integer m_ints[16];
    
    public method operator [](integer n) -> integer {
        return this.m_ints[n];
    }
    public method operator []=(integer n,integer inte) {
        this.m_ints[n]=inte;
        this.m_arrLength+=1;
    }
    
    
    public method toArray(integer size) {
        integer temp = this.m_value;
        integer temp2 = 0;
        integer decimal = 0;
        integer i;
        if (size<=0) {
            return;
        }
        if (temp==0) {
            this[0]=0;
            return;
        }
        this.m_arrElementSize = size;
        decimal = R2I(Pow(10,size));
        for (i=0; temp!=0; i+=1) {
            temp2 = temp;
            temp /= decimal;
            this[i] = temp2 - temp * decimal;
        }
    }
    
    public static method create(integer value) -> Integer {
        Integer this = Integer.allocate();
        //debug BJDebugMsg("create Integer: " +I2S(this));
        this.m_value = value;
        this.m_length = IntegerLength(value);
        if (value<0) {
            this.m_negative = -1;
        } else {
            this.m_negative = 1;
        }
        return this;
    }
    
    public method destroy() {
        integer i;
        for (i=0; i<this.m_length; i+=1) {
            this[i]=0;
        }
        this.m_negative=1;
        this.m_value=0;
        this.m_length=0;
        this.m_arrLength=0;
        this.deallocate();
    }
    
    public static method toFixedWidthString(integer value, integer len) -> string {
        integer size = len-IntegerLength(value);
        integer i;
        string str="";
        if (size>0) {
            for (i=0; i<size; i+=1) {
                str+="0";
            }
            str+=I2S(value);
        } else {
            str=I2S(value);
        }
        return str;
    }
    
    public method toString(integer len) -> string {
        return toFixedWidthString(this.m_value, len);
    }
    
    
    public method toArrayString(integer size) -> string {
        string str = "";
        integer i;
        this.toArray(size);
        for (i=0; i<this.m_arrLength; i+=1) {
            if (i==this.m_arrLength-1) {
                str+=(I2S(this[i]));
            } else {
                str+=(I2S(this[i])+",");
            }
        }
        return str;
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Integer.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/String.zinc
public struct String {
    public string m_value="";
    public integer m_length=0;
    public integer m_arrLength=0;
    //单个数组元素含有数字的个数
    public integer m_arrElementSize=0;
    private string m_strs[16];

    public static method create(string value) -> String {
        String this = String.allocate();
        debug BJDebugMsg("create String: " +I2S(this));
        this.m_value = value;
        this.m_length = StringLength(value);
        return this;
    }

    public method operator [](integer n) -> string {
        return this.m_strs[n];
    }
    
    public method operator []=(integer n,string str) {
        this.m_strs[n] = str;
        this.m_arrLength += 1;
    }
    
    public method destroy() {
        integer i;
        for (i=0; i<this.m_length; i+=1) {
            this[i]=null;
        }
        this.m_value=null;
        this.m_arrLength=0;
        this.deallocate();
    }
    public method toArray(integer size) {
        integer i;
        integer index=0;
        if (size<=0) {
            return;
        }
        if (size>this.m_length) {
            this[index] = this.m_value;
            return;
        }
        this.m_arrElementSize = size;
        for (i=0; i<this.m_length; i+=size) {
            if ((i+size)<=this.m_length) {
                this[index] = SubString(this.m_value,i,i+size);
            } else {
                this[index] = SubString(this.m_value,i,this.m_length);
            }
            index+=1;
        }
    }
    public method toArrayString(integer size) -> string {
        string str = "";
        integer i;
        this.toArray(size);
        for (i=0; i<this.m_arrLength; i+=1) {
            if (i==this.m_arrLength-1) {
                str+=(this[i]);
            } else {
                str+=(this[i]+",");
            }
        }
        return str;
    }
    //adds a string to another string in spaced intervals
    //StringJoin("1234567","-",3,0) -> "123-456-7"
    public static method join(string s, string str, integer spacing, integer start) ->string {
        integer i = StringLength(s);
        integer p;
        for (p=1; p*spacing+start<i; p+=1) {
            s = SubString(s,0,p*spacing+p+start-1)+str+SubString(s,p*spacing+p+start-1,StringLength(s));
        }
        return s;
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/String.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Point.zinc
public struct Point {
    public real m_x;
    public real m_y;

    public static method create(real x, real y) -> Point {
        Point this =  Point.allocate();
        this.m_x = x;
        this.m_y = y;
        return this;
    }

    public method setXY(real x, real y) {
        this.m_x = x;
        this.m_y = y;
    }

    public method destroy() {
        this.m_x = 0;
        this.m_y = 0;
        this.deallocate();
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Point.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Dto.zinc
type Dto_units extends unit[32];
type Dto_effects extends effect[32];
type Dto_reals extends real[32];
type Dto_integers extends integer[32];
type Dto_strings extends string[32];
type Dto_booleans extends boolean[32];


public struct Dto {
    public timer m_timer = null;
    public trigger m_trigger = null;
    public player m_player = null;
    public unit m_unit = null;
    public unit m_unitB = null;
    public item m_item = null;
    public item m_itemB = null;
    public effect m_effect = null;
    public string m_string = null;
    public real m_real = 0.00;
    public real m_realB = 0.00;
    public integer m_integer = 0;
    public integer m_integerB = 0;
    public integer m_data = 0;
    public boolean m_boolean = false;
    public group m_group = null;
    public texttag m_texttag = null;
    public lightning m_lightning = null;
    public triggeraction m_action = null;
    public triggercondition m_condition = null;
    
    public Dto_effects m_effects = 0;
    public Dto_units m_units = 0;
    public Dto_reals m_reals = 0;
    public Dto_integers m_integers = 0;
    public Dto_strings m_strings = 0;
    public Dto_booleans m_booleans = 0;
    
    public static method create() -> Dto {
        Dto this = Dto.allocate();
        debug BJDebugMsg("The Number of 'Dto' is :"+I2S(this));
        return this;
    }
    
    private method hasArray() -> boolean {
        if (this.m_effects == 0 && this.m_units == 0 && this.m_reals == 0 && this.m_integers == 0 && this.m_strings == 0 && this.m_booleans == 0) {
            return false;
        }
        return true;
    }
    
    private method destroyArray() {
        integer i;
        for (i = 0; i < 32; i += 1) {
            this.m_effects[i] = null;
            this.m_units[i] = null;
            this.m_strings[i] = null;
        }
        this.m_effects.destroy();
        this.m_units.destroy();
        this.m_reals.destroy();
        this.m_integers.destroy();
        this.m_strings.destroy();
        this.m_effects = 0;
        this.m_units = 0;
        this.m_reals = 0;
        this.m_integers = 0;
        this.m_strings = 0;
        this.m_booleans = 0;
    }
    
    public method destroy() {
        ReleaseTimer(this.m_timer);
        ReleaseTrigger(this.m_trigger);
        ReleaseGroup(this.m_group);
        this.m_timer=null;
        this.m_effect=null;
        this.m_unit=null;
        this.m_unitB=null;
        this.m_item=null;
        this.m_itemB=null;
        this.m_trigger=null;
        this.m_group=null;
        this.m_texttag=null;
        if (this.hasArray()) {
            this.destroyArray();
        }
        this.deallocate();
    }
    
    public method delayDestroy(real timeout) {
        DelayExecuteFunc(timeout, this, function() {
            Dto this = GetDelayTimerData();
            this.destroy();
        });
    }
    
    
    public method getEffects(integer index) -> effect {
        return this.m_effects[index];
    }
    
    public method setEffects(integer index, effect eff) {
        if (this.m_effects == 0) {
            this.m_effects = Dto_effects.create();
        }
        this.m_effects[index] = eff;
    }
    
    
    public method getUnits(integer index) -> unit {
        return this.m_units[index];
    }
    
    public method setUnits(integer index, unit u) {
        if (this.m_units == 0) {
            this.m_units = Dto_units.create();
        }
        this.m_units[index] = u;
    }
    
    
    public method getReals(integer index) -> real {
        return this.m_reals[index];
    }
    
    public method setReals(integer index, real r) {
        if (this.m_reals == 0) {
            this.m_reals = Dto_reals.create();
        }
        this.m_reals[index] = r;
    }
    
    
    public method getIntegers(integer index) -> integer {
        return this.m_integers[index];
    }
    
    public method setIntegers(integer index, integer i) {
        if (this.m_integers == 0) {
            this.m_integers = Dto_integers.create();
        }
        this.m_integers[index] = i;
    }
    
    
    public method getStrings(integer index) -> string {
        return this.m_strings[index];
    }
    
    public method setStrings(integer index, string str) {
        if (this.m_strings == 0) {
            this.m_strings = Dto_strings.create();
        }
        this.m_strings[index] = str;
    }
    
    
    public method getBooleans(integer index) -> boolean {
        return this.m_booleans[index];
    }
    
    public method setBooleans(integer index, boolean bl) {
        if (this.m_booleans == 0) {
            this.m_booleans = Dto_booleans.create();
        }
        this.m_booleans[index] = bl;
    }
    
    public method getDataTimer(integer data) -> timer {
        if (this.m_timer == null) {
            this.m_timer = NewTimer();
            SetTimerData(this.m_timer,data);
        }
        return this.m_timer;
    }

    public method getTimer() -> timer {
        return getDataTimer(this);
    }

    public method getDataTrigger(integer data) -> trigger {
        if (this.m_trigger == null) {
            this.m_trigger = NewTrigger();
            SetTriggerData(this.m_trigger,data);
        }
        return this.m_trigger;
    }

    public method getTrigger() -> trigger {
        return getDataTrigger(this);
    }
        
    public method getGroup() -> group {
        if (this.m_group == null) {
            this.m_group = NewGroup();
        }
        return this.m_group;
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Dto.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Property.zinc
// package entity

public struct Property {
    //力量
    public integer m_strength = 0;
    //敏捷
    public integer m_agility = 0;
    //智力
    public integer m_intelligence = 0;
    //攻击
    public real m_attack = 0.00;
    //护甲
    public real m_armor = 0.00;
    //生命
    public real m_HP = 0.00;
    //魔法
    public real m_MP = 0.00;
    //生命恢复率
    public real m_regenHP = 0.00;
    //魔法恢复率
    public real m_regenMP = 0.00;
    //攻击速度
    public real m_attackSpeed = 0.00;
    //移动速度
    public real m_moveSpeed = 0.00;
    //暴击值
    public real m_critValue = 0.00;
    //暴击率
    public real m_critChance = 0.00;
    //命中率
    public real m_hitChance = 0.00;
    //闪避率
    public real m_dodgeChance = 0.00;
    //魔抗
    public real m_magicResistRate = 0.00;
    //伤害效果率
    public real m_damageRate = 0.00;

    
    private static method onInit() {
        Property d = 0;
        d.m_critValue = 1.00;
        d.m_hitChance = 1.00;
        d.m_damageRate = 1.00;
    }

    public static method create() -> Property {
        Property d = Property.allocate();
        return d;
    }

    public method clone(Property target) {
        target.m_strength = this.m_strength;
        target.m_agility = this.m_agility;
        target.m_intelligence = this.m_intelligence;
        target.m_attack = this.m_attack;
        target.m_armor = this.m_armor;
        target.m_HP = this.m_HP;
        target.m_MP = this.m_MP;
        target.m_regenHP = this.m_regenHP;
        target.m_regenMP = this.m_regenMP;
        target.m_attackSpeed = this.m_attackSpeed;
        target.m_moveSpeed = this.m_moveSpeed;
        target.m_critValue = this.m_critValue;
        target.m_critChance = this.m_critChance;
        target.m_hitChance = this.m_hitChance;
        target.m_dodgeChance = this.m_dodgeChance;
        target.m_magicResistRate = this.m_magicResistRate;
        target.m_damageRate = this.m_damageRate;
    }

    public method add(Property target) {
        this.m_strength += target.m_strength;
        this.m_agility += target.m_agility;
        this.m_intelligence += target.m_intelligence;
        this.m_attack += target.m_attack;
        this.m_armor += target.m_armor;
        this.m_HP += target.m_HP;
        this.m_MP += target.m_MP;
        this.m_regenHP += target.m_regenHP;
        this.m_regenMP += target.m_regenMP;
        this.m_attackSpeed += target.m_attackSpeed;
        this.m_moveSpeed += target.m_moveSpeed;
        this.m_critValue += target.m_critValue;
        this.m_critChance += target.m_critChance;
        this.m_hitChance += target.m_hitChance;
        this.m_dodgeChance += target.m_dodgeChance;
        this.m_magicResistRate += target.m_magicResistRate;
        this.m_damageRate += target.m_damageRate;
    }

    public method minus(Property target) {
        this.m_strength -= target.m_strength;
        this.m_agility -= target.m_agility;
        this.m_intelligence -= target.m_intelligence;
        this.m_attack -= target.m_attack;
        this.m_armor -= target.m_armor;
        this.m_HP -= target.m_HP;
        this.m_MP -= target.m_MP;
        this.m_regenHP -= target.m_regenHP;
        this.m_regenMP -= target.m_regenMP;
        this.m_attackSpeed -= target.m_attackSpeed;
        this.m_moveSpeed -= target.m_moveSpeed;
        this.m_critValue -= target.m_critValue;
        this.m_critChance -= target.m_critChance;
        this.m_hitChance -= target.m_hitChance;
        this.m_dodgeChance -= target.m_dodgeChance;
        this.m_magicResistRate -= target.m_magicResistRate;
        this.m_damageRate -= target.m_damageRate;
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Property.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/BuffType.zinc

public struct BuffType {
    delegate Property m_property;
    
    //buffID
    public integer m_buffId = 0;
    //buffAbilityId
    public integer m_buffAbilityId = 0;
    //buff总等级
    public integer m_buffLevel = 1;
    //图标路径
    public string m_icon = "Objects\\RandomObject\\RandomObject.mdl";
    //特效A路径
    public string m_effectPathA = "";
    //特效A附着点
    public string m_attachA = "";
    //特效B路径
    public string m_effectPathB = "";
    //特效B附着点
    public string m_attachB = "";
    //特效C路径
    public string m_effectPathC = "";
    //特效C附着点
    public string m_attachC = "";
    //是否可驱散
    public boolean m_enableDispel = false;
    //是否可被盗取
    public boolean m_enablePurloin = false;

    

    public static method get(integer buffId) -> thistype {
        return g_p_ht_type.loadInteger(StringHash("BuffType"),buffId);
    }
    
    public static method create(integer buffId) -> thistype {
        thistype this = thistype.get(buffId);
        if (this > 0) {
            return this;
        }
        this = thistype.allocate();

        this.m_property = Property.create();
        g_p_ht_type.saveInteger(StringHash("BuffType"), buffId, this);
        return this;
    }

    public method destroy() {
        this.m_enableDispel = false;
        this.m_property.destroy();
        this.deallocate();
    }

    public method clone(thistype target) {
        target.m_buffId = this.m_buffId;
        target.m_buffAbilityId = this.m_buffAbilityId;
        target.m_buffLevel = this.m_buffLevel;
        target.m_icon = this.m_icon;
        target.m_effectPathA = this.m_effectPathA;
        target.m_attachA = this.m_attachA;
        target.m_effectPathB = this.m_effectPathB;
        target.m_attachB = this.m_attachB;
        target.m_effectPathC = this.m_effectPathC;
        target.m_attachC = this.m_attachC;
        target.m_enableDispel = this.m_enableDispel;
        target.m_enablePurloin = this.m_enablePurloin;
        this.m_property.clone(target.m_property);
    }

    public method cloneNew() -> thistype {
        thistype new = thistype.allocate();
        new.m_property = Property.create();
        this.clone(new);
        return new;
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/BuffType.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/AbilityType.zinc

public struct AbilityType {
    public integer m_abilityId = 0;
    //GUI图标ID
    public integer m_distId = 0;
    //指令ID
    public integer m_orderId = 0;
    //目标类型
    public integer m_targetType = 0;
    //热键
    public string m_hotkey = "";
    //图标路径
    public string m_icon = "";
    //名称
    public string m_name = "";
    //描述
    public string m_desc = "";
    //投射物模型路径
    public string m_missileModel = "";
    //投射物尺寸
    public real m_missileScale = 1.00;
    //投射物速度
    public real m_missileSpeed = 0.00;
    //投射物弧度
    public real m_missileArc = 0.00;
    //投射物跟踪
    public boolean m_missileHoming = true;
    //动画序号
    public integer m_animotionIndex = 0;
    //施法者特效路径
    public string m_effectCaster = "";
    //目标单位特效路径
    public string m_effectTarget = "";
    //目标点特效路径
    public string m_effectTargetPoint = "";
    //施法者特效附着点
    public string m_attachCaster = "";
    //目标单位特效附着点
    public string m_attachTarget = "";
    //目标点特效高度
    public real m_attachTargetPointHeight = 0.00;
    //buffID
    public integer m_buffId = 0;
    //施法时间
    public real m_timeCast = 0.00;
    //冷却时间
    public real m_timeCD = 0.00;
    //效果持续时间
    public real m_timeEffectDurative = 0.00;
    
    public real m_timeDurative = 0.00;
    //魔法消耗
    public real m_costMP = 0.00;
    //最小施法距离
    public real m_distanceMin = 0.00;
    //最大施法距离
    public real m_distanceMax = 0.00;
    //影响范围
    public real m_rangeEffect = 0.00;
    //默认伤害值
    public real m_damageDefault = 0.00;
    
    public static method get(integer abilityId) -> AbilityType {
        return g_p_ht_type.loadInteger(StringHash("AbilityType"),abilityId);
    }
    
    public static method create(integer abilityId) -> AbilityType {
        AbilityType this = AbilityType.get(abilityId);
        if (this > 0) {
            return this;
        }
        this = AbilityType.allocate();
        g_p_ht_type.saveInteger(StringHash("AbilityType"), abilityId, this);
        return this;
    }

    public method destroy() {
        this.deallocate();
    }

    public method clone(AbilityType target) {
        target.m_abilityId = this.m_abilityId;
        target.m_distId = this.m_distId;
        target.m_orderId = this.m_orderId;
        target.m_targetType = this.m_targetType;
        target.m_hotkey = this.m_hotkey;
        target.m_icon = this.m_icon;
        target.m_name = this.m_name;
        target.m_desc = this.m_desc;
        target.m_missileModel = this.m_missileModel;
        target.m_missileScale = this.m_missileScale;
        target.m_missileSpeed = this.m_missileSpeed;
        target.m_missileArc = this.m_missileArc;
        target.m_missileHoming = this.m_missileHoming;
        target.m_animotionIndex = this.m_animotionIndex;
        target.m_effectCaster = this.m_effectCaster;
        target.m_effectTarget = this.m_effectTarget;
        target.m_effectTargetPoint = this.m_effectTargetPoint;
        target.m_attachCaster = this.m_attachCaster;
        target.m_attachTarget = this.m_attachTarget;
        target.m_attachTargetPointHeight = this.m_attachTargetPointHeight;
        target.m_buffId = this.m_buffId;
        target.m_timeCast = this.m_timeCast;
        target.m_timeCD = this.m_timeCD;
        target.m_timeEffectDurative = this.m_timeEffectDurative;
        target.m_timeDurative = this.m_timeDurative;
        target.m_costMP = this.m_costMP;
        target.m_distanceMin = this.m_distanceMin;
        target.m_distanceMax = this.m_distanceMax;
        target.m_rangeEffect = this.m_rangeEffect;
        target.m_damageDefault = this.m_damageDefault;
    }

    public method cloneNew() -> AbilityType {
        AbilityType new = AbilityType.allocate();
        this.clone(new);
        return new;
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/AbilityType.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/ItemType.zinc

public struct ItemType {
    delegate Property m_property;
    
    //模型路径
    public string m_model = "Objects/InventoryItems/TreasureChest/treasurechest.mdl";
    //图标
    public string m_icon = "ReplaceableTextures/CommandButtons/BTNSelectHeroOn.blp";
    //名称
    public string m_name = "";
    //物品ID
    public integer m_itemId = 0;
    //GUI图标ID
    public integer m_distId = 0;
    //装备类型
    public integer m_equipTypeId = 0;
    //金币价格
    public integer m_cost = 0;
    //物品描述
    public string m_desc = "";
    //物品额外描述
    public string m_exDesc = "";
    
    private static method setData(integer itemId, ItemType d) {
        g_p_ht_type.saveInteger(StringHash("ItemType"),itemId,d);
    }

    public static method get(integer itemId) -> ItemType {
        return g_p_ht_type.loadInteger(StringHash("ItemType"),itemId);
    }
    
    

    public static method create(integer itemId) -> ItemType {
        ItemType this = ItemType.get(itemId);
        if (this > 0) {
            return this;
        }
        this = ItemType.allocate();
        this.m_property = Property.create();
        setData(itemId, this);
        return this;
    }

    public method destroy() {
        this.m_property.destroy();
        this.deallocate();
    }

    public method clone(ItemType target) {
        target.m_model = this.m_model;
        target.m_icon = this.m_icon;
        target.m_name = this.m_name;
        target.m_itemId = this.m_itemId;
        target.m_distId = this.m_distId;
        target.m_equipTypeId = this.m_equipTypeId;
        target.m_cost = this.m_cost;
        target.m_desc = this.m_desc;
        this.m_property.clone(target.m_property);
    }

    public method cloneNew() -> ItemType {
        ItemType new = ItemType.allocate();
        new.m_property = Property.create();
        this.clone(new);
        return new;
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/ItemType.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/UnitType.zinc

public struct UnitType {
    delegate Property m_property;
    
    //单位ID
    public integer m_unitId = 0;
    //作为目标类型
    public string m_asTargetType = "";
    //护甲类型
    public integer m_armorType = 0;
    //攻击类型
    public integer m_attackType = 0;
    //基础护甲值
    public real m_armorBasic = 0.00;
    //基础攻击值
    public real m_attackBasic = 0.00;
    //攻击间隔
    public real m_attackInterval = 0.00;
    //模型路径
    public string m_model = "";
    //图标路径
    public string m_icon = "";
    //模型尺寸
    public real m_modelScale = 1.00;
    //死亡时间
    public real m_timeDeath = 0.00;
    //英雄类型
    public string m_heroType = "STR";

    //残影单位ID
    public integer m_unitIdShadow = 0;
    //在GUI中的模型尺寸
    public real m_modelScaleGUI = 1.00;
    //技能条的高度
    public real m_heightSpellbar = 160.00;
    //技能施放ready动作序号
    public integer m_animoutionSpellready = 0;
    //技能施放end动作序号
    public integer m_animoutionSpellend = 0;

    private static method setData(integer unitId, UnitType d) {
        g_p_ht_type.saveInteger(StringHash("UnitType"),unitId,d);
    }

    public static method get(integer unitId) -> UnitType {
        return g_p_ht_type.loadInteger(StringHash("UnitType"),unitId);
    }
    
    

    public static method create(integer unitId) -> UnitType {
        UnitType this = UnitType.get(unitId);
        if (this > 0) {
            return this;
        }
        this = UnitType.allocate();
        this.m_property = Property.create();
        this.m_asTargetType = "ground";
        this.m_model = "Objects\\RandomObject\\RandomObject.mdl";
        this.m_icon = "ReplaceableTextures\\CommandButtons\\BTNSelectHeroOn.blp";
        this.m_modelScale = 1.00;
        this.m_timeDeath = 0.00;
        this.m_heroType = "STR";
        this.m_unitIdShadow = 0;
        this.m_modelScaleGUI = 0.08;
        this.m_heightSpellbar = 160.00;
        this.m_animoutionSpellready = 0;
        this.m_animoutionSpellend = 0;
        setData(unitId, this);
        return this;
    }

    public method destroy() {
        this.m_property.destroy();
        this.deallocate();
    }

    public method clone(UnitType target) {
        target.m_unitId = this.m_unitId;
        target.m_asTargetType = this.m_asTargetType;
        target.m_armorType = this.m_armorType;
        target.m_attackType = this.m_attackType;
        target.m_armorBasic = this.m_armorBasic;
        target.m_attackBasic = this.m_attackBasic;
        target.m_attackInterval = this.m_attackInterval;
        target.m_model = this.m_model;
        target.m_icon = this.m_icon;
        target.m_modelScale = this.m_modelScale;
        target.m_timeDeath = this.m_timeDeath;
        target.m_regenHP = this.m_regenHP;
        target.m_regenMP = this.m_regenMP;
        target.m_heroType = this.m_heroType;
        target.m_unitIdShadow = this.m_unitIdShadow;
        target.m_modelScaleGUI = this.m_modelScaleGUI;
        target.m_heightSpellbar = this.m_heightSpellbar;
        target.m_animoutionSpellready = this.m_animoutionSpellready;
        target.m_animoutionSpellend = this.m_animoutionSpellend;
        this.m_property.clone(target.m_property);
    }

    public method cloneNew() -> UnitType {
        UnitType new = UnitType.allocate();
        new.m_property = Property.create();
        this.clone(new);
        return new;
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/UnitType.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/IBuff.zinc
public interface IBuff {
    public integer m_buffId;

    method getBuff() -> Buff;

    method removeFromUnit();

    method ondestroy();

    method addToUnit(Unit ud);

}

public type IBuffList extends IBuff[16];
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/IBuff.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Buff.zinc

public struct Buff {
    delegate BuffType m_buffType;
    
    public Unit m_ud;

    public integer m_buffId;

    public integer m_buffLevel = 0;

    public integer m_maxLevel = 10;

    public real m_duration = 0.0;

    public timer m_timer = null;

    public effect m_effectA = null;
    public effect m_effectB = null;
    public effect m_effectC = null;
    
    public method clear() {
        UnitRemoveAbility(this.m_ud.m_unit, this.m_buffAbilityId);
        UnitRemoveAbility(this.m_ud.m_unit, this.m_buffId);
        RefreshUnitUI(this.m_ud.m_unit);
        DestroyEffect(this.m_effectA);
        DestroyEffect(this.m_effectB);
        DestroyEffect(this.m_effectC);
        this.m_effectA = null;
        this.m_effectB = null;
        this.m_effectC = null;
    }

    public method destroy() {
        ReleaseTimer(this.m_timer);
        this.clear();
        this.m_timer = null;
        this.m_buffType.destroy();
        this.deallocate();
    }
    
    public method effect() {
        unit u = this.m_ud.m_unit;
        integer abId = this.m_buffAbilityId;
        if (abId != 0) {
            if (GetUnitAbilityLevel(u, abId) <= 0) {
                UnitAddAbilityPermanent(u, abId);
            }
        }

        if (m_effectPathA != "") {
            DestroyEffect(m_effectA);
            m_effectA = AddSpecialEffectTarget(m_effectPathA, u, m_attachA);
        }
        if (m_effectPathB != "") {
            DestroyEffect(m_effectB);
            m_effectB = AddSpecialEffectTarget(m_effectPathB, u, m_attachB);
        }
        if (m_effectPathC != "") {
            DestroyEffect(m_effectC);
            m_effectC = AddSpecialEffectTarget(m_effectPathC, u, m_attachC);
        }
        RefreshUnitUI(u);
        u = null;
    }

    public method setBuffLevel(integer level) {
        unit u = m_ud.m_unit;
        integer abId = m_buffAbilityId;
        if (level != 0) {
            m_buffLevel += level;
            if (m_buffLevel <= 0) {
                m_buffLevel = 1;
            } else if (m_buffLevel > m_maxLevel) {
                m_buffLevel = m_maxLevel;
            }
            SetUnitAbilityLevel(u, abId, m_buffLevel);
        }
        u = null;
    }

    public method addToUnit(Unit ud) {
        if (m_ud > 0 && ud != m_ud) {
            this.clear();
        }
        this.m_ud = ud;
    }

    public method setDuration(real time) {
        this.m_duration = time;
    }

    public method getTimer(integer data) -> timer {
        if (this.m_timer == null) {
            this.m_timer = NewTimer();
            SetTimerData(this.m_timer, data);
        }
        return this.m_timer;
    }

    public static method create(integer buffId) -> Buff {
        Buff this = Buff.allocate();
        debug BJDebugMsg("create Buff : " + I2S(this) );
        this.m_buffType = BuffType.get(buffId).cloneNew();
        return this;
    }
    
}

public type BuffList extends Buff[16];
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Buff.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Ability.zinc

public struct Ability {
    delegate AbilityType m_abilityType;
    
    public integer m_abilityId;
    public Unit m_ud;

    public static method get(Unit ud, integer abilityId) -> Ability {
        return g_p_ht.loadInteger(ud, abilityId);
    }
    
    public static method create(Unit ud, integer abilityId) -> Ability {
        Ability this = Ability.get(ud, abilityId);
        if (this > 0) {
            return this;
        }
        this = Ability.allocate();
        debug BJDebugMsg("create Ability : " + I2S(this) );
        this.m_ud = ud;
        this.m_abilityType = AbilityType.get(abilityId).cloneNew();
        this.m_abilityId = abilityId;
        UnitAddAbilityPermanent(ud.m_unit, abilityId);
        g_p_ht.saveInteger(ud, abilityId, this);
        return this;
    }
    
    public method destroy() {
        UnitRemoveAbility(this.m_ud.m_unit,this.m_abilityId);
        this.m_abilityType.destroy();
        g_p_ht.removeSavedInteger(this.m_ud, this.m_abilityId);
        this.deallocate();
    }
}

public type AbilityList extends Ability[32];
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Ability.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Item.zinc

public struct Item {
    delegate ItemType m_itemType;
    
    public item m_item;

    private Unit m_ud;
    
    private static Item s_data[];

    //private static Table s_data;

    public static method get(item it) -> Item {
        return s_data[GetItemId(it)];
    }

    public method destroy() {
        debug BJDebugMsg("destroy Item: " + GetItemName(this.m_item));
        s_data[GetItemId(this.m_item)] = 0;
        //RemoveItem(this.m_item);
        this.m_ud = 0;
        this.m_item = null;
        this.m_itemType.destroy();
        this.deallocate();
    }
    
    public static method create(item it) -> Item {
        Item this = Item.get(it);
        if (this > 0) {
            if (GetItemTypeId(it) == this.m_itemId) {
                return this;
            } else {
                this.destroy();
            }
        }
        this = Item.allocate();
        this.m_item = it;
        this.m_itemType = ItemType.get(GetItemTypeId(it)).cloneNew();
        //this.m_itemType.m_cost = GetItemGoldCostById(this.m_itemType.m_itemId);
        debug BJDebugMsg("create Item : "+ GetItemName(it) + I2S(this) + ",item name in database:" + this.m_name);
        s_data[GetItemId(it)] = this;
        return this;
    }

    public static method init() {
        //s_data = Table.create();
    }
    
    public method setOwner(Unit ud) {
        this.m_ud = ud;
    }
    
    public method getOwner() -> Unit {
        return this.m_ud;
    }
    
    public method isNull() -> boolean {
        return GetItemTypeId(this.m_item) == 0;
    }
    
}

public type SmallItemList extends Item[32];
public type ItemList extends Item[64];
public type LargeItemList extends Item[128];
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Item.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Unit.zinc

public struct Unit {
    delegate UnitType m_unitType;
    
    public unit m_unit;
    
    private static Unit s_data[];
    
    // 一秒的攻击速度值
    private static real s_baseAttackSpeed = 250;
    // 最小攻击间隔
    private static real s_minInv = 0.25;

    private method propInit(unit u) {
        this.m_unitType = UnitType.get(GetUnitTypeId(u)).cloneNew();
        this.m_moveSpeed = GetUnitMoveSpeed(u);
    }

    public static method get(unit u) -> Unit {
        Unit this = s_data[GetUnitId(u)];
        if (this > 0 && this.m_unitType <= 0) {
            this.propInit(u);
        }
        return this;
    }
    
    public static method create(unit u) -> Unit {
        Unit this = Unit.get(u);
        if (this > 0) {
            return this;
        }
        this = Unit.allocate();
        this.m_unit = u;

        this.propInit(u);

        debug BJDebugMsg("create Unit : " + I2S(this) );
        s_data[GetUnitId(u)] = this;
        return this;
    }
    
    public method destroy() {
        s_data[GetUnitId(m_unit)] = 0;
        DestroyMoveSpeedStruct(m_unit);
        RemoveUnit(m_unit);
        this.m_unit = null;
        this.m_unitType.destroy();
        this.deallocate();
    }
    
    public method isUnitTypeHero() -> boolean {
        return IsUnitType(this.m_unit, UNIT_TYPE_HERO);
    }
    
    public method getAttackInterval() -> real {
        if (this.isUnitTypeHero()) {
            return this.m_attackInterval - GetHeroAgi(this.m_unit, true) * 0.001;
        }
        return this.m_attackInterval;
    }
    
    public method setAttackSpeed(real speed) {
        if (speed > (s_baseAttackSpeed / s_minInv)) {
            this.m_attackInterval = s_minInv;
        } else if (speed < 0) {
            this.m_attackInterval = 0;
        } else {
            this.m_attackInterval = s_baseAttackSpeed / speed;
        }
    }
    
    public method getAttackSpeed() -> real {
        real inv = this.getAttackInterval();
        real result = s_baseAttackSpeed;
        if (inv != 0) {
            result = s_baseAttackSpeed / inv;
        }
        return result;
    }
    
    public method addAttackSpeed(real speed) {
        this.setAttackSpeed(getAttackSpeed() + speed);
    }
    
    public method addAttackSpeedByPercent(real percent) {
        this.addAttackSpeed(getAttackSpeed() * percent);
    }
    
    public method setMoveSpeed(real value) {
        this.m_moveSpeed = value;
        if (value > 1500) {
            SetUnitMoveSpeedX(this.m_unit, 1500);
        } else if (value < 0) {
            SetUnitMoveSpeedX(this.m_unit, 0);
        } else {
            SetUnitMoveSpeedX(this.m_unit, value);
        }
    }
    
    public method getMoveSpeed() -> real {
        //return GetUnitMoveSpeedX(m_unit);
        return this.m_moveSpeed;
    }
    
    public method addMoveSpeed(real value) {
        real newSpeed = 0;
        if (value != 0) {
            newSpeed = getMoveSpeed() + value;
            this.setMoveSpeed(newSpeed);
        }
    }

    public method addMoveSpeedTimed(real value, real delay, real timeout) {
        Dto dto = Dto.create();
        dto.m_data = integer(this);
        dto.m_real = value;
        dto.m_realB = timeout;

        TimerStart(dto.getTimer(), delay, false, function () {
            Dto dto = GetTimerData(GetExpiredTimer());
            Unit ud = Unit(dto.m_data);
            ud.addMoveSpeed(dto.m_real);
            if (dto.m_realB > 0) {
                TimerStart(dto.getTimer(), dto.m_realB, false, function () {
                    Dto dto = GetTimerData(GetExpiredTimer());
                    Unit ud = Unit(dto.m_data);
                    ud.addMoveSpeed(-dto.m_real);
                    dto.destroy();
                });
            } else {
                ud.addMoveSpeed(-dto.m_real);
                dto.destroy();
            }
        });
    }

    public method getRegenHP() -> real {
        if (this.isUnitTypeHero()) {
            return this.m_regenHP + GetHeroStr(this.m_unit, true) * 0.05;
        }
        return this.m_regenHP;
    }
    
    public method addRegenHP(real hp) {
        this.m_regenHP += hp;
    }
    
    public method getRegenMP() -> real {
        if (this.isUnitTypeHero()) {
            return this.m_regenMP + GetHeroInt(this.m_unit, true) * 0.05;
        }
        return this.m_regenMP;
    }
    
    public method addRegenMPByPercent(real percent) {
        this.m_regenMP += (this.m_regenMP * percent);
    }

}

public type UnitList extends Unit[64];
public type LargeUnitList extends Unit[128];
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Unit.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Hero.zinc

public struct Hero {
    delegate Unit m_ud;
    
    private static Hero s_data[];
    
    module StructList;
    
    public static method get(Unit ud) -> Hero {
        return s_data[integer(ud)];
    }
    
    public static method create(Unit ud) -> Hero {
        Hero this = Hero.get(ud);
        if (this > 0) {
            return this;
        }
        this = Hero.allocate();
        debug BJDebugMsg("create Hero : " + I2S(this) );
        this.m_ud = ud;
        s_data[integer(ud)] = this;
        this.listAdd();
        return this;
    }
    
    public method destroy() {
        s_data[integer(this.m_ud)] = 0;
        this.listRemove();
        this.deallocate();
    }
    
    public method getMainNatureValue() -> integer {
        string tp = this.m_heroType;
        if (tp == "STR") {
            return GetHeroStr(this.m_unit, true);
        } else if (tp == "AGI") {
            return GetHeroAgi(this.m_unit, true);
        } else if (tp == "INT") {
            return GetHeroInt(this.m_unit, true);
        }
        return 0;
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Hero.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Playor.zinc
// package Sysbase


public struct Playor {
    public player m_player = null;
    public integer m_playerId = 0;
    
    private static Playor s_data[];
    
    public static method get(player p) -> Playor {
        return s_data[GetPlayerId(p)];
    }

    public static method create(player p) -> Playor {
        Playor this = Playor.allocate();
        debug BJDebugMsg("create Playor : " + I2S(this) );
        this.m_player = p;
        this.m_playerId = GetPlayerId(p);
        s_data[GetPlayerId(p)] = this;
        return this;
    }
    
    public method destroy() {
        s_data[GetPlayerId(this.m_player)] = 0;
        this.m_player = null;
        this.deallocate();
    }
    
    public static method init() {
        Playor d;
        integer i;
        for (i = 0; i < bj_MAX_PLAYERS; i += 1) {
            if (IsPlayerPlaying(Player(i))) {
                d = Playor.create(Player(i));
            }
        }
    }
    
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Playor.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/UnitGroup.zinc
public struct UnitGroup {
    public List m_unitList;

    public static method create() -> UnitGroup {
        UnitGroup this =  UnitGroup.allocate();
        this.m_unitList = List.create();

        debug BJDebugMsg("create UnitGroup :"+I2S(this));

        return this;
    }

    public method destroy() {
        this.m_unitList.destroy();
        this.deallocate();
    }

    private method searchUnit(Unit ud) -> Unit {
        Link link;
        Unit tmpUd = 0;

        if (ud <= 0) {
            return 0;
        }

        link = this.m_unitList.search(ud);

        if (link > 0 && link.data > 0) {
            tmpUd = Unit(link.data);
        }
        return tmpUd;
    }

    public method getFirstLink() -> Link {
        return this.m_unitList.first;
    }

    public method isUnitInGroup(Unit ud) -> boolean {
        if (ud <= 0) return false;
        if (searchUnit(ud) > 0) {
            return true;
        }
        return false;
    }

    public method addUnit(Unit ud) -> UnitGroup {
        if (!isUnitInGroup(ud)) {
            Link.create(this.m_unitList, ud);
        }
        return this;
    }

    public method addAll(UnitGroup grp) -> UnitGroup {
        Unit ud;
        Link link = grp.getFirstLink();
        while (link > 0) {
            ud = Unit(link.data);
            this.addUnit(ud);
            link = link.next;
        }
        return this;
    }

    public method removeUnit(Unit ud) -> UnitGroup {
        Link link = this.m_unitList.search(ud);
        link.destroy();
        return this;
    }

    public method clear() -> UnitGroup {
        this.m_unitList.clear();
        return this;
    }

    public method enumUnits(real x, real y, real radius) -> UnitGroup {
        bj_groupEnumTypeId = this;
        EnumUnitsInRange(x, y, radius, function () {
            Unit ud = Unit.get(GetFilterUnit());
            UnitGroup this = UnitGroup(bj_groupEnumTypeId);
            this.addUnit(ud);
        });
        return this;
    }

    public static method createEnumUnitGroup(real x, real y, real radius) -> UnitGroup {
        UnitGroup ugrp = UnitGroup.create();
        ugrp.enumUnits(x, y, radius);
        return ugrp;
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/UnitGroup.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/BoardWrapper.zinc
public struct BoardWrapper {
    delegate Board m_board;
    public boolean m_isShow = false;
    public boolean m_isClearing = false;
    public Playor m_pd;
    
    private static BoardWrapper s_data[];
    
    private static method setData(Playor pd, BoardWrapper d) {
        s_data[integer(pd)] = d;
    }
    
    public static method get(Playor pd) -> BoardWrapper {
        return s_data[integer(pd)];
    }
    
    public static method create(Playor pd) -> BoardWrapper {
        BoardWrapper this = BoardWrapper.get(pd);
        if (this > 0) {
            return this;
        }
        this = BoardWrapper.allocate();
        debug BJDebugMsg("create BoardWrapper : " + I2S(this) );
        this.m_pd = pd;
        this.m_board = Board.create();
        this.title= "";
        this.titleColor= 0xFFFF00;
        this.minimized[pd.m_player] = false;
        this.visible[pd.m_player] = false;
        setData(this.m_pd, this);
        return this;
    }
    
    public method destroy() {
        setData(this.m_pd, 0);
        this.deallocate();
    }
    
    public static method getIntegerString(Integer ind, integer index) -> string {
        if (index == ind.m_arrLength - 1) {
            return I2S(ind[index]);
        } else {
            return Integer.toFixedWidthString(ind[index], ind.m_arrElementSize);
        }
    }
    
    public static method getTextWidth(Integer ind, integer index) -> real {
        integer n = ind[index];
        integer j;
        if (index == ind.m_arrLength - 1) {
            for (j=0; n!=0; j+=1) {
                n= n/10;
            }
            return 0.005*j;
        }
        return 0.015;
    }
    
    public static method getStringValue(Board board, BoardItem it, integer col, integer row, real value, integer color) -> integer {
        Integer ind = Integer.create(R2I(value));
        integer j;
        integer i;
        
        //将数字按每三个一组拆分并储存到数组
        ind.toArray(3);
        j=ind.m_arrLength - 1;
        for (i=0; i<=j; i+=1) {
            it = board[i+col][row];
            it.text= getIntegerString(ind, j-i);
            it.color= color;
            it.width= getTextWidth(ind, j-i);
            it.setDisplay(true,false);
        }
        ind.destroy();
        return i+col;
    }
    
    public method setBoardVisible(boolean isVisible) {
        this.visible[this.m_pd.m_player] = isVisible;
    }
    
    public method setBoardMinimize(boolean isMini) {
        this.minimized[this.m_pd.m_player] = isMini;
    }
    
    public method clearBoard() {
        this.m_isClearing = true;
        DelayExecuteFunc(0.13,this,function() {
            BoardWrapper this = GetDelayTimerData();
            this.m_isClearing = false;
        });
        this.title= "";
        this.titleColor= 0xFFFF00;
        this.clear();
    }
    
    public static method clearAllBoard() {
        integer i;
        for (i = 0; i < bj_MAX_PLAYERS; i += 1) {
            if (IsPlayerPlaying(Player(i)) && IsPlayerControllerIsUser(Player(i))) {
                BoardWrapper.get(Playor.get(Player(i))).clearBoard();
            }
        }
    }
    
    public static method init() {
        Playor pd;
        integer i;
        for (i = 0; i < bj_MAX_PLAYERS; i += 1) {
            pd = Playor.get(Player(i));
            if (IsPlayerControllerIsUser(pd.m_player)) {
                BoardWrapper.create(pd);
            }
        }
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/BoardWrapper.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/DialogWrapper.zinc
// package entity

public struct DialogWrapper {
    public Dialog m_dialog;
    public boolean m_isBusyDialog = false;
    public Playor m_pd;
    
    private static thistype s_data[];
    
    public static method get(Playor pd) -> thistype {
        return s_data[integer(pd)];
    }
    
    public static method create(Playor pd) -> thistype {
        thistype this = thistype.get(pd);
        if (this > 0) {
            return this;
        }
        this = thistype.allocate();
        
        this.m_pd = pd;
        this.m_dialog = Dialog.create();
        this.m_dialog.SetCarryData(this);
        s_data[integer(pd)] = this;
        return this;
    }
    
    public method destroy() {
        s_data[integer(this.m_pd)] = 0;
        this.deallocate();
    }

    
    public static method init() {
        Playor pd;
        integer i;
        for (i = 0; i < bj_MAX_PLAYERS; i += 1) {
            pd = Playor.get(Player(i));
            if (IsPlayerControllerIsUser(pd.m_player)) {
                thistype.create(pd);
            }
        }
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/DialogWrapper.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Damage.zinc
public struct Damage {
    public Unit m_ud;

    public integer m_searchIndex = -1;

    public boolean m_isAttack = true; // 是否攻击伤害
    public boolean m_isRanged = false; // 是否远程伤害

    public attacktype m_attackType = ATTACK_TYPE_NORMAL;        //法术
    public damagetype m_damageType = DAMAGE_TYPE_NORMAL;        //普通(计算护甲)
    public weapontype m_weaponType = WEAPON_TYPE_WHOKNOWS;      //无

    public boolean m_isShowEff = false;
    public string m_effPath = "Abilities\\Spells\\Other\\Stampede\\StampedeMissileDeath.mdl";
    public string m_attachPointName = "origin";

    module StructList;

    public static method get(Unit ud, integer searchIndex) -> thistype {
        thistype dam = thistype.first;
        while (dam > 0) {
            if (dam.m_ud == ud  && dam.m_searchIndex != -1 && dam.m_searchIndex == searchIndex) {
                return dam;
            }
            dam = dam.next;
        }
        return 0;
    }

    public method destroy() {
        this.m_effPath = null;
        this.listRemove();
        this.deallocate();
    }

    public static method create(Unit ud) -> thistype {
        thistype this =  thistype.allocate();

        debug BJDebugMsg("create Damage:"+I2S(this));

        this.m_ud = ud;
        this.listAdd();
        return this;
    }

    public method showDamageEffect(boolean isShow) {
        this.m_isShowEff = isShow;
    }

    public method damageUnit(Unit utd, real damage) {
        unit u = m_ud.m_unit,ut = utd.m_unit;
        real critChance = m_ud.m_critChance;
        real critValue = m_ud.m_critValue;

        UnitDamageTarget(u, ut, damage, m_isAttack, m_isRanged, m_attackType, m_damageType, m_weaponType);

        if (m_isShowEff) {
            AddEffectTarget(m_effPath, ut, m_attachPointName);
        }

        u = null;
        ut = null;
    }

    public method damageWidget(widget target, real damage) {
        unit u = m_ud.m_unit;
        real critChance = m_ud.m_critChance;
        real critValue = m_ud.m_critValue;

        UnitDamageTarget(u, target, damage,m_isAttack,m_isRanged,m_attackType,m_damageType,m_weaponType);

        if (m_isShowEff) {
            AddEffectTarget(m_effPath, target, m_attachPointName);
        }

        u = null;
    }

    public method damagePoint(real x, real y, real radius, real damage) {
        unit u = m_ud.m_unit;
        UnitDamagePoint(u, 0, radius, x, y, damage, m_isAttack, m_isRanged, m_attackType, m_damageType, m_weaponType);
        u = null;
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Damage.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Spell.zinc

public struct Spell {
    delegate Dto m_dto;

    public real m_x;
    public real m_y;

    public real m_targetX;
    public real m_targetY;

    public Vector m_velocity = 0; // 速度
    public Vector m_position = 0; // 位置
    public Vector m_acc = 0; // 加速度(acceleration)
    public static Vector s_g; // 重力加速度
    public Vector m_helpVector = 0; // 辅助向量

    public real m_angle;

    public real m_damage;

    public real m_interval;

    public integer m_count;
    public integer m_countA;
    public integer m_countB;
    
    public Ability m_ad;
    public Unit m_ud;

    public Damage m_damd;

    private UnitGroup m_uGrp = 0;

    private static Spell s_data[];

    public static method get(Ability ad) -> Spell {
        return s_data[integer(ad)];
    }

    public static method create(Ability ad) -> Spell {
        Spell this = Spell.get(ad);
        if (this > 0) {
            return this;
        }
        this = Spell.allocate();
        debug BJDebugMsg("create Spell : " + I2S(this) );
        this.m_ad = ad;
        this.m_ud = ad.m_ud;
        this.m_damd = Damage.create(m_ud);
        this.m_dto = Dto.create();
        this.m_count = 0;
        this.m_countA = 0;
        this.m_countB = 0;
        s_data[integer(this.m_ad)] = this;
        return this;
    }
    
    public method destroy() {
        this.m_damd.destroy();
        this.m_dto.destroy();
        this.m_uGrp.destroy();
        this.m_acc.destroy();
        this.m_velocity.destroy();
        this.m_position.destroy();
        this.m_helpVector.destroy();
        s_data[integer(this.m_ad)] = 0;
        this.deallocate();
    }

    public static method init() {
        s_g = Vector.create(0.0, 0.0, -800.0*FPS_50*FPS_50);
    }

    public method getUnitGroup() -> UnitGroup {
        if (m_uGrp > 0) {
            return m_uGrp;
        }
        m_uGrp = UnitGroup.create();
        return m_uGrp;
    }

    public method getTimer() -> timer {
        if (this.m_timer == null) {
            this.m_timer = NewTimer();
            SetTimerData(this.m_timer,this);
        }
        return this.m_timer;
    }

    public method getTrigger() -> trigger {
        if (this.m_trigger == null) {
            this.m_trigger = NewTrigger();
            SetTriggerData(this.m_trigger, this);
        }
        return this.m_trigger;
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/Spell.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/SpellBar.zinc

public struct SpellBar {
    public unit m_bar;

    public Unit m_owner;

    public CallBack m_callBack;

    private Dto m_dto;

    private static SpellBar s_data[];

    public static method get(Unit owner) -> SpellBar {
        return s_data[integer(owner)];
    }

    public method destroy() {
        RemoveUnit(this.m_bar);
        s_data[integer(this.m_owner)] = 0;
        this.m_dto.destroy();
        this.m_bar = null;
        this.m_callBack = 0;
        this.m_owner = 0;
        this.m_dto = 0;
        this.deallocate();
    }

    
    public method start(real aniDurationTime, integer aniStartIndex, real aniStartSpeed, integer aniEndIndex, real aniEndSpeed) {
        Dto dto = this.m_dto;
        dto.m_unit = this.m_owner.m_unit;
        dto.m_integerB = aniEndIndex;
        dto.m_real = aniEndSpeed;
        SetUnitTimeScale(dto.m_unit, aniStartSpeed);
        if (aniStartIndex >= 0) {
            SetUnitAnimationByIndex(dto.m_unit, aniStartIndex);
        }
        if (aniDurationTime > 0) {
            TimerStart(dto.getTimer(), aniDurationTime, false, function () {
                Dto dto = GetTimerData(GetExpiredTimer());
                SetUnitAnimationByIndex(dto.m_unit, dto.m_integerB);
                SetUnitTimeScale(dto.m_unit, dto.m_real);
            });
        }
    }

    public method end() {
        SetUnitTimeScale(this.m_owner.m_unit, 1.00);
        this.destroy();
    }

    private method newBar(real duration, boolean isNormal, real height) {
        unit u = this.m_owner.m_unit;
        this.m_bar = CreateUnit(GetOwningPlayer(u), 'h004', GetUnitX(u), GetUnitY(u), 0.00);
        SetUnitFlyable(this.m_bar);
        SetUnitFlyHeight(this.m_bar, height, 0);

        if (isNormal) {
            SetUnitAnimation(this.m_bar, "attack");
        } else {
            SetUnitAnimation(this.m_bar, "spell");
        }
        SetUnitTimeScale(this.m_bar, 10.00/duration);
        this.m_dto.m_integer = R2I(duration/0.02);

        TimerStart(this.m_dto.getTimer(), 0.02, true, function () {
            Dto dto = GetTimerData(GetExpiredTimer());
            SpellBar this = SpellBar(dto.m_data);
            unit u = this.m_owner.m_unit;
            if (dto.m_integer > 0) {
                dto.m_integer -= 1;
                SetUnitX(this.m_bar, GetUnitX(u));
                SetUnitY(this.m_bar, GetUnitY(u));
            } else {
                this.m_callBack.evaluate(this.m_owner);
                this.end();
                IssueImmediateOrderById(u, ORDER_STOP);
            }
            u = null;
        });
        u = null;
    }

    
    public static method create(Unit owner, real duration, boolean isNormal, real height, CallBack callBack) -> SpellBar {
        SpellBar this = SpellBar.get(owner);
        if (this > 0) {
            return this;
        }
        this = SpellBar.allocate();
        debug BJDebugMsg("create SpellBar : " + I2S(this) );
        this.m_owner = owner;
        this.m_callBack = callBack;
        this.m_dto = Dto.create();

        this.m_dto.m_data = this;
        s_data[integer(owner)] = this;

        this.newBar(duration, isNormal, height);
        return this;
    }

    public static method init() {
        trigger tr = CreateTrigger();
        TriggerRegisterAllPlayerEvent(tr, EVENT_PLAYER_UNIT_SPELL_ENDCAST, function() {
            Unit ud = Unit.get(GetTriggerUnit());
            SpellBar spellBar = SpellBar.get(ud);

            if (spellBar > 0) {
                spellBar.end();
            }
        });
        tr = null;
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/SpellBar.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/UnitItems.zinc
public struct UnitItems {
    public Unit m_ud;

    public integer m_moduleId;

    public ItemList m_itemList;

    public integer m_itemSize;

    private static UnitItems s_data[128][64];

    public static method get(Unit ud, integer moduleId) -> UnitItems {
        return s_data[integer(ud)][moduleId];
    }

    public static method create(Unit ud, integer moduleId) -> UnitItems {
        UnitItems this =  UnitItems.get(ud, moduleId);
        if (this > 0) {
            return this;
        }
        this = UnitItems.allocate();
        debug BJDebugMsg("new UnitItems : " +I2S(this));

        this.m_ud = ud;
        this.m_moduleId = moduleId;
        this.m_itemList = ItemList.create();
        
        s_data[ud][moduleId] = this;
        return this;
    }

    public method destroy() {
        s_data[this.m_ud][this.m_moduleId] = 0;
        this.m_itemList.destroy();
        this.m_itemList = 0;
        this.deallocate();
    }

    public method setSize(integer size) {
        this.m_itemSize = size;
    }

    public method getItem(integer index) -> Item {
        Item itd = 0;
        if (index >= 0 && index < this.m_itemSize) {
            itd = this.m_itemList[index];
        }
        return itd;
    }

    public method getEmptyIndex() -> integer {
        Item tpItem;
        integer index = -1,i;

        for (i = 0; i < this.m_itemSize; i+=1) {
            tpItem = this.m_itemList[i];
            if (integer(tpItem) <= 0) {
                index = i;
                break;
            }
        }
        debug BJDebugMsg("UnitItems getEmptyIndex: " +I2S(index));
        
        return index;
    }
    
    public method addItemByIndex(Item itd, integer index) {
        debug BJDebugMsg("UnitItems add item: " +I2S(itd) + "index:" + I2S(index) + "m_itemSize:" + I2S(this.m_itemSize));
        if (index >= 0 && index < this.m_itemSize) {
            this.m_itemList[index] = itd;
        }
    }
    
    public method addItem(Item itd) -> integer {
        integer index = this.getEmptyIndex();
        this.addItemByIndex(itd, index);
        return index;
    }

    public method removeItem(Item itd) {
        Item tpItem;
        integer i;

        if (integer(itd) <= 0) {
            return;
        }

        for (i = 0; i < this.m_itemSize; i+=1) {
            tpItem = this.m_itemList[i];
            if (tpItem == itd) {
                this.m_itemList[i] = 0;
                break;
            }
        }
    }

    public method removeItemByIndex(integer index) {
        Item itd = this.getItem(index);
        this.removeItem(itd);
    }

    public method isFull() -> boolean {
        if (this.getEmptyIndex() < 0) {
            return true;
        }
        return false;
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/UnitItems.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/UnitBuffs.zinc
public struct UnitBuffs {
    public Unit m_ud;

    public integer m_moduleId;

    public IBuffList m_buffList;

    public integer m_buffSize;

    private static thistype s_data[2048][4];

    public static integer C_BUFF_TYPE_NOMAL = 0; // 普通

    public static method get(Unit ud, integer moduleId) -> thistype {
        return s_data[integer(ud)][moduleId];
    }

    public static method create(Unit ud, integer moduleId) -> thistype {
        thistype this =  thistype.get(ud, moduleId);
        if (this > 0) {
            return this;
        }
        this = thistype.allocate();
        debug BJDebugMsg("new UnitBuffs : " +I2S(this));

        this.m_ud = ud;
        this.m_moduleId = moduleId;
        this.m_buffList = IBuffList.create();
        
        s_data[ud][moduleId] = this;
        return this;
    }

    public method destroy() {
        s_data[m_ud][m_moduleId] = 0;
        this.m_buffList.destroy();
        this.m_buffList = 0;
        this.deallocate();
    }

    private method getEmptyIndex() -> integer {
        IBuff tp;
        integer index = -1,i;

        for (i = 0; i < this.m_buffSize; i+=1) {
            tp = this.m_buffList[i];
            if (integer(tp) <= 0) {
                index = i;
            }
        }
        debug BJDebugMsg("UnitBuffs getEmptyIndex: " +I2S(index));
        
        return index;
    }

    public method getBuffIndex(integer buffId) -> integer {
        IBuff ibuff;
        integer index = -1,i;

        if (buffId > 0) {
            for (i = 0; i < this.m_buffSize; i+=1) {
                ibuff = m_buffList[i];
                if (buffId == ibuff.m_buffId) {
                    index = i;
                    break;
                }
            }
        }

        return index;
    }

    public method removeBuff(integer buffId) {
        integer idx = this.getBuffIndex(buffId);
        IBuff tp;

        if (idx > 0) {
            tp = m_buffList[idx];
            tp.ondestroy();
            m_buffList[idx] = 0;
        }
    }

    public method addBuff(IBuff bufd) -> integer {
        integer index = this.getBuffIndex(bufd.m_buffId);
        if (index < 0) {
            index = this.getEmptyIndex();
            if (index >= 0 && index < this.m_buffSize) {
                this.m_buffList[index] = bufd;
            }
        } else {
            
        }
        return index;
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/UnitBuffs.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\base/UnitAbilities.zinc
public struct UnitAbilities {
    public Unit m_ud;

    public integer m_moduleId;

    public AbilityList m_abilityList;

    public integer m_abilitySize;

    private static thistype s_data[128][64];

    public static method get(Unit ud, integer moduleId) -> thistype {
        return s_data[integer(ud)][moduleId];
    }

    public static method create(Unit ud, integer moduleId) -> thistype {
        thistype this =  thistype.get(ud, moduleId);
        if (this > 0) {
            return this;
        }
        this = thistype.allocate();
        debug BJDebugMsg("new UnitAbilities : " +I2S(this));

        this.m_ud = ud;
        this.m_moduleId = moduleId;
        this.m_abilityList = AbilityList.create();
        
        s_data[ud][moduleId] = this;
        return this;
    }

    public method destroy() {
        s_data[this.m_ud][this.m_moduleId] = 0;
        this.m_abilityList.destroy();
        this.m_abilityList = 0;
        this.deallocate();
    }

    public method setSize(integer size) {
        this.m_abilitySize = size;
    }

    public method getAbility(integer index) -> Ability {
        Ability abd = 0;
        if (index >= 0 && index < this.m_abilitySize) {
            abd = this.m_abilityList[index];
        }
        return abd;
    }

    public method getEmptyIndex() -> integer {
        Ability tp;
        integer index = -1,i;

        for (i = 0; i < this.m_abilitySize; i+=1) {
            tp = this.m_abilityList[i];
            if (integer(tp) <= 0) {
                index = i;
            }
        }
        debug BJDebugMsg("UnitAbilities getEmptyIndex: " +I2S(index));
        
        return index;
    }
    
    public method addAbilityByIndex(Ability abd, integer index) {
        debug BJDebugMsg("UnitAbilities add item: " +I2S(abd) + "index:" + I2S(index) + "m_abilitySize:" + I2S(this.m_abilitySize));
        if (index >= 0 && index < this.m_abilitySize) {
            this.m_abilityList[index] = abd;
        }
    }
    
    public method addAbility(Ability abd) -> integer {
        integer index = this.getEmptyIndex();
        this.addAbilityByIndex(abd, index);
        return index;
    }

    public method removeAbility(Ability abd) {
        Ability tp;
        integer i;

        if (integer(abd) <= 0) {
            return;
        }

        for (i = 0; i < this.m_abilitySize; i+=1) {
            tp = this.m_abilityList[i];
            if (tp == abd) {
                this.m_abilityList[i] = 0;
                break;
            }
        }
    }

    public method removeAbilityByIndex(integer index) {
        Ability abd = this.getAbility(index);
        this.removeAbility(abd);
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/UnitAbilities.zinc
    
    
    private function init() {
        Item.init();
        Playor.init();
        BoardWrapper.init();
        DialogWrapper.init();

        Spell.init();
        SpellBar.init();
    }
    
    private function intiItem(unit u, item it) {
        integer i;
        
        if (GetItemType(it)!=ITEM_TYPE_POWERUP && GetItemTypeId(it) != XE_DUMMY_ITEMID) {
            Item.create(it);
        }
    }
    
    private function initUnit(unit u) {
        Unit ud;
        Hero hero;
        debug BJDebugMsg(GetUnitName(u)+" with ID "+I2S(GetUnitId(u))+" entered the map.");
        ud = Unit.create(u);
        if (ud.isUnitTypeHero()) {
            hero = Hero.create(ud);
            if (MAP_CONTROL_USER == GetPlayerController(GetOwningPlayer(ud.m_unit))) {
            }
        }
    }
    private function releaseUnit(unit u) {
        Unit ud = Unit.get(u);
        debug BJDebugMsg(GetUnitName(u)+" with ID "+I2S(GetUnitId(u))+" left the map.");
        if (ud.isUnitTypeHero()) {
            Hero.get(ud).destroy();
            if (MAP_CONTROL_USER == GetPlayerController(GetOwningPlayer(ud.m_unit))) {
            }
        }
        ud.destroy();
    }
    
    private function onInit() {
        initPlayerColor();

        g_p_ht_type=DataCache.create("beanType");
        g_p_ht=DataCache.create("bean");

        //单位进入地图注册单位
        OnUnitIndexed(initUnit);
        OnUnitDeindexed(releaseUnit);
        //单位拾起物品注册物品
        RegisterPickupItemResponse(intiItem);

        RegisterGameStartResponse(init);
    }
}
//! endzinc
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\base/_base.zn
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/_utils.zn
//! zinc
library utils requires base {
    
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/StringUtils.zinc
public struct StringUtils[] {

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/StringUtils.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/MathUtils.zinc
public struct MathUtils[] {
    
     public static method getRandomLocationInRangeXY(real x,real y,real r) -> location {
        real R = GetRandomReal(0.00,r/2);
        real A = GetRandomReal(0.00,2*bj_PI);
        real X = x + R*Cos(A);
        real Y = y + R*Sin(A);
        return Location(X,Y);
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/MathUtils.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/PlayerUtils.zinc
public struct PlayerUtils[] {

    public static method displayTimedTextToPlayer(player toPlayer, real duration, string message) {
        DisplayTimedTextToPlayer(toPlayer, 0, 0, duration, message);
    }

    public static method dislayTextToPlayer(player toPlayer, string message) {
        DisplayTextToPlayer(toPlayer, 0, 0, message);
    }

    public static method dislayTimedTextToAllPlayer(real duration, string message) {
        DisplayTimedTextToForce(bj_FORCE_ALL_PLAYERS, duration, message);
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/PlayerUtils.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/EffectUtils.zinc
public struct EffectUtils[] {
    
    public static method addLightningAtUnitToXYTimed(string se,unit u,real dx,real dy,real timeout) {
        Dto dto = Dto.create();
        dto.m_unit = u;
        dto.setReals(0, dx);
        dto.setReals(1, dy);
        dto.m_integer = R2I(timeout/0.02);
        dto.m_integerB = dto.m_integer;
        dto.m_lightning = AddLightningEx(se,false,GetUnitX(u),GetUnitY(u),GetUnitZ(u)+60.00, dx,dy,GetPointZ(dx,dy));

        TimerStart(dto.getTimer(), 0.02, true, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            MoveLightningEx(dto.m_lightning, false, GetUnitX(dto.m_unit), GetUnitY(dto.m_unit), GetUnitZ(dto.m_unit)+60.00, dto.getReals(0), dto.getReals(1), GetPointZ(dto.getReals(0), dto.getReals(1)));
            dto.m_integer -= 1;
            if (dto.m_integer < 0) {
                DestroyLightning(dto.m_lightning);
                dto.m_lightning=null;
                dto.destroy();
            } else if (dto.m_integer <= dto.m_integerB && dto.m_integer > 0) {
                SetLightningColor(dto.m_lightning, 1, 1, 1, I2R(dto.m_integer)/I2R(dto.m_integerB) );
            }
        });
    }
    
    public static method addLightningAtUnitToUnitTimed(string se,unit ua,unit ub,real timeout) {
        Dto dto = Dto.create();
        dto.m_unit = ua;
        dto.m_unitB = ub;
        dto.m_integer = R2I(timeout/0.02);
        dto.m_data = dto.m_integer;
        dto.m_lightning = AddLightningEx(se,false,GetUnitX(ua),GetUnitY(ua),GetUnitZ(ua)+60.00,GetUnitX(ub),GetUnitY(ub),GetUnitZ(ub)+60.00);

        TimerStart(dto.getTimer(), 0.02, true, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            MoveLightningEx(dto.m_lightning, false, GetUnitX(dto.m_unit), GetUnitY(dto.m_unit), GetUnitZ(dto.m_unit)+60.00,GetUnitX(dto.m_unitB), GetUnitY(dto.m_unitB), GetUnitZ(dto.m_unitB)+60.00);
            dto.m_integer-=1;
            if (dto.m_integer < 0) {
                DestroyLightning(dto.m_lightning);
                dto.m_lightning=null;
                dto.destroy();
            } else if (dto.m_integer <= dto.m_data && dto.m_integer > 0) {
                SetLightningColor(dto.m_lightning, 1, 1, 1, I2R(dto.m_integer)/I2R(dto.m_data) );
            }
        });
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/EffectUtils.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/UnitUtils.zinc
public constant integer ORDER_STOP = 851972;
public constant integer ORDER_ATTACK = 851983;
public constant integer ORDER_RIGHT_CLICK = 851971; //右键点击
public constant integer ORDER_GUARD = 851993; //保持警戒位置
public constant integer ORDER_MOVE = 851986;

public struct UnitUtils[] {

    
    public static method delayIssueImmediateOrderById(unit u, integer orderId, real delay) {
        Dto dto = Dto.create();
        dto.m_unit = u;
        dto.m_integer = orderId;
        TimerStart(dto.getTimer(), delay, false, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            IssueImmediateOrderById(dto.m_unit,dto.m_integer);
            dto.destroy();
        });
    }
    
    
    public static method delayIssueTargetOrderById(unit u, integer orderId, unit ua, real delay) {
        Dto dto = Dto.create();
        dto.m_unit = u;
        dto.m_unitB = ua;
        dto.m_integer = orderId;
        TimerStart(dto.getTimer(), delay, false, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            IssueTargetOrderById(dto.m_unit, dto.m_integer, dto.m_unitB);
            dto.destroy();
        });
    }

    
    public static method setUnitPosition(unit whichUnit, Vector position) {
        SetUnitX(whichUnit, position.x);
        SetUnitY(whichUnit, position.y);
        if (position.z > 0) {
            SetUnitFlyHeight(whichUnit, position.z, 0.00);
        }
    }

    public static method setUnitFlyHeight(unit whichUnit, real scale) {
        SetUnitFlyable(whichUnit);
        SetUnitFlyHeight(whichUnit, scale, 0.00);
    }
    
    public static method setUnitScale(unit whichUnit, real scale) {
        SetUnitScale(whichUnit, scale, scale, scale);
    }

    public static method isEnemyOfNpc(unit u) -> boolean {
        return !IsUnitAlly(u, PLAYER_NPC);
    }
    
    public static method addUnitBonusTimed(unit u, Bonus whichBonus, integer value, real timeout) {
        Dto dto = Dto.create();
        dto.m_unit = u;
        dto.m_data = integer(whichBonus);
        dto.m_integer = value;

        AddUnitBonus(u,whichBonus,value);
        TimerStart(dto.getTimer(), timeout, false, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            Bonus whichBonus = dto.m_data;
            AddUnitBonus(dto.m_unit,whichBonus,-dto.m_integer);
            dto.destroy();
        });
    }
    
    public static method delayRemoveUnit(unit u, real delay) {
        Dto dto = Dto.create();
        dto.m_unit = u;

        TimerStart(dto.getTimer(), delay, false, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            Unit.get(dto.m_unit).destroy();
            RemoveUnit(dto.m_unit);
            dto.destroy();
        });
    }
    
    public static method delayAddWidgetLife(unit u, real rec, real delay) {
        Dto dto = Dto.create();
        dto.m_unit = u;
        dto.m_real = rec;

        TimerStart(dto.getTimer(), delay, false, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            real currLife=GetWidgetLife(dto.m_unit);
            SetWidgetLife(dto.m_unit,currLife+dto.m_real);
            dto.destroy();
        });
    }
    
    public static method reduceUnitDamaged(unit u, real dm, real rate) {
        Dto dto = Dto.create();
        real maxLife = GetUnitState(u,UNIT_STATE_MAX_LIFE);
        real currLife = GetUnitState(u,UNIT_STATE_LIFE);
        real realDamage = dm * (1 - rate);
        real recover = dm * rate;
        dto.m_unit = u;
        dto.m_real = currLife;
        dto.setReals(0, realDamage);
        dto.setReals(1, recover);

        if (realDamage < maxLife) {
            if (dm >= currLife && realDamage < currLife) {
                SetUnitInvulnerable(dto.m_unit,true);
                TimerStart(dto.getTimer(), 0.00, false, function() {
                    Dto dto = GetTimerData(GetExpiredTimer());
                    real currLife = dto.m_real;
                    real realDamage = dto.getReals(0);
                    SetUnitInvulnerable(dto.m_unit, false);
                    SetWidgetLife(dto.m_unit, currLife - realDamage);
                    dto.destroy();
                });
            } else if (recover > (maxLife - currLife)) {
                TimerStart(dto.getTimer(), 0.00, false, function() {
                    Dto dto = GetTimerData(GetExpiredTimer());
                    real currLife = GetWidgetLife(dto.m_unit);
                    real recover = dto.getReals(1);
                    SetWidgetLife(dto.m_unit, currLife + recover);
                    dto.destroy();
                });
            } else {
                SetWidgetLife(u, currLife + recover);
                dto.destroy();
            }
        }
    }
    
    private static method delaySetUnitState(unit u, integer state, real newValue, real delay) {
        Dto dto = Dto.create();
        dto.m_unit = u;
        dto.m_integer = state;
        dto.m_real = newValue;

        TimerStart(dto.getTimer(), delay, false, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            unitstate whichUnitState = UNIT_STATE_LIFE;
            if (dto.m_integer == 0) {
                whichUnitState = UNIT_STATE_LIFE;
            } else if (dto.m_integer == 1) {
                whichUnitState = UNIT_STATE_MANA;
            }
            SetUnitState(dto.m_unit, whichUnitState, RMaxBJ(0, dto.m_real));
            dto.destroy();
            whichUnitState = null;
        });
    }
    
    public static method delaySetUnitLife(unit u, real newValue, real delay) {
        delaySetUnitState(u, 0, newValue, delay);
    }
    
    public static method delaySetUnitMana(unit u, real newValue, real delay) {
        delaySetUnitState(u, 1, newValue, delay);
    }
    
    public static method setUnitInvulnerableTimed(unit u, real timeout) {
        Dto dto = Dto.create();
        dto.m_unit = u;
        SetUnitInvulnerable(u, true);
        TimerStart(dto.getTimer(), timeout, false, function() {
            Dto dto = GetExpiredTimerData();
            SetUnitInvulnerable(dto.m_unit, false);
            dto.destroy();
        });
    }
    
    public static method protectUnitFromHarmTimed(unit u, real timeout) {
        // TODO
    }
    
    public static method delaySetUnitAnimation(unit u, integer index, real time, real resetTime) {
        Dto dto = Dto.create();
        dto.m_unit = u;
        dto.m_integer = index;
        dto.m_real = resetTime;

        DisableUnitTimed(u, time + resetTime);

        TimerStart(dto.getTimer(), time, false, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            SetUnitAnimationByIndex(dto.m_unit,dto.m_integer);
            if (dto.m_real > 0) {
                UnitUtils.delayResetUnitAnimation(dto.m_unit, dto.m_real);
            }
            dto.destroy();
        });
    }

    public static method delayResetUnitAnimation(unit u, real time) {
        Dto dto = Dto.create();
        dto.m_unit = u;
        TimerStart(dto.getTimer(), time, false, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            ResetUnitAnimation(dto.m_unit);
            dto.destroy();
        });
    }

    
    public static method unitJumpTo(unit u,real angle,real distance,real lasttime,real heightMax,real dcHeight,integer data,CallBack callback) {
        Dto dto = Dto.create();
        integer lastLength = R2I(lasttime/0.02);
        real step,stepX,stepY;

        SetUnitFlyable(u);
        dto.m_unit = u;
        dto.m_data = data;
        dto.m_integer = integer(callback);
        dto.setReals(0, angle);
        dto.setReals(1, GetUnitX(u));
        dto.setReals(2, GetUnitY(u));
        dto.setReals(3, heightMax);
        
        step = distance/lastLength;
        dto.setReals(4, step);
        dto.setReals(5, 1.0/lastLength);
        dto.setReals(6, GetUnitFlyHeight(u));
        dto.setReals(7, dcHeight);

        stepX = step*Cos(angle*bj_DEGTORAD);
        stepY = step*Sin(angle*bj_DEGTORAD);
        dto.setReals(8, stepX);
        dto.setReals(9, stepY);

        dto.setIntegers(0, lastLength);
        dto.setIntegers(1, 0);

        TimerStart(dto.getTimer(), 0.02, true, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            real angle = dto.getReals(0);
            real heightMax = dto.getReals(3);
            real step = dto.getReals(4);
            real stepX = dto.getReals(8);
            real stepY = dto.getReals(9);
            real recR = dto.getReals(5);
            real flyHeight = dto.getReals(6);
            integer count = dto.getIntegers(1);
            real X = 0.00;
            real Y = 0.00;
            real Z = 0.00;
            real Hdc = 0.00;
            real height = 0.00;
            CallBack callback;
            
            if (count < dto.getIntegers(0)) {
                count+=1;
                dto.setIntegers(1, count);
                X = dto.getReals(1) + count*stepX;
                Y = dto.getReals(2) + count*stepY;
                Hdc = GetUnitZ(dto.m_unit)-GetPointZ(X,Y);

                if ((Hdc > -dto.getReals(7)) && !(IsBeyondBound(X,Y))) {
                    SetUnitX(dto.m_unit,X);
                    SetUnitY(dto.m_unit,Y);
                    height=(-(2*I2R(count)*recR-1)*(2*I2R(count)*recR-1)+1)*heightMax+flyHeight;
                    SetUnitFlyHeight(dto.m_unit, height,0.00);
                } else {
                    SetUnitFlyHeight(dto.m_unit, flyHeight, 0.00);
                    callback = CallBack(dto.m_integer);
                    if (callback!=0) {
                        callback.evaluate(dto.m_data);
                    }
                    dto.destroy();
                }
            } else {
                callback=dto.m_integer;
                if (callback!=0) {
                    callback.evaluate(dto.m_data);
                }
                SetUnitFlyHeight(dto.m_unit, flyHeight, 0.00);
                dto.destroy();
            }
        });
    }

    private static real c_l_charge_interval = 0.02;
    
    public static method unitChargeToUnit(unit u,unit ua,integer faceModel,real initSpeed,real maxSpeed,real acc,string eff,real et,integer data,CallBack callback) {
        Dto dto = Dto.create();
        real dis = GetDistanceByUnitXY(u, ua);
        real avgspeed = (initSpeed+maxSpeed)/2;
        dto.m_unit = u;
        dto.m_unitB = ua;
        dto.m_data = data;
        dto.m_integer = integer(callback);
        dto.m_string = eff;
        dto.setReals(0, initSpeed*c_l_charge_interval);
        dto.setReals(1, maxSpeed*c_l_charge_interval);
        dto.setReals(2, acc*c_l_charge_interval*c_l_charge_interval);
        dto.setReals(3, et);
        dto.setIntegers(0, R2I(dis/(avgspeed*c_l_charge_interval)));
        dto.setIntegers(1, faceModel);

        TimerStart(dto.getTimer(),c_l_charge_interval,true,function () {
            Dto dto = GetTimerData(GetExpiredTimer());
            real ux = GetUnitX(dto.m_unit);
            real uy = GetUnitY(dto.m_unit);
            real step = dto.getReals(0);
            real maxStep = dto.getReals(1);
            real et = dto.getReals(3);
            integer count = dto.getIntegers(0);
            integer faceModel = dto.getIntegers(1);
            real ang = GetAngleByXY(ux, uy, GetUnitX(dto.m_unitB),GetUnitY(dto.m_unitB));
            real dis = GetDistanceByXY(ux, uy, GetUnitX(dto.m_unitB),GetUnitY(dto.m_unitB));
            real x;
            real y;
            CallBack callback = 0;
            
            if (count > 0) {
                count -= 1;
                dto.setIntegers(0, count);
                if (step < maxStep) {
                    step += dto.getReals(2);
                    dto.setReals(0, step);
                } else {
                    dto.setReals(0, maxStep);
                }
                x = ux + GetDistanceX(step, ang);
                y = uy + GetDistanceY(step, ang);
                SetUnitX(dto.m_unit, x);
                SetUnitY(dto.m_unit, y);

                if (faceModel == 0) {
                    SetUnitFacing(dto.m_unit, ang);
                } else if (faceModel == 1) {
                    SetUnitFacing(dto.m_unit, ang + 180.0);
                }

                if (dto.m_string!=null && et > 0.0) {
                    if (ModuloInteger(count,R2I(et/c_l_charge_interval))==0) {
                        DestroyEffect(AddSpecialEffectTarget(dto.m_string, dto.m_unit, "origin"));
                    }
                }
                if (dis < XE_MAX_COLLISION_SIZE) {
                    callback = dto.m_integer;
                    if (callback!=0) {
                        callback.evaluate(dto.m_data);
                    }
                    dto.destroy();
                }
            } else {
                callback = dto.m_integer;
                if (callback!=0) {
                    callback.evaluate(dto.m_data);
                }
                dto.destroy();
            }
        });
    }
   
    public static method unitChargeToXY(unit u,real x,real y,integer faceModel,real initSpeed,real maxSpeed,real acc, boolean isTerrian,boolean isObstacle,string eff,real et,integer data,CallBack callback) {
        Dto dto = Dto.create();
        real dis = GetDistanceByXY(GetUnitX(u),GetUnitY(u),x,y);
        real avgspeed = (initSpeed+maxSpeed)/2;
        dto.m_unit = u;
        dto.m_string = eff;
        dto.m_integer = integer(callback);
        dto.m_data = data;
        dto.setIntegers(0, R2I(dis/(avgspeed*c_l_charge_interval)));
        dto.setIntegers(1, faceModel);
        dto.setReals(0, x);
        dto.setReals(1, y);
        dto.setReals(2, initSpeed*c_l_charge_interval);
        dto.setReals(3, maxSpeed*c_l_charge_interval);
        dto.setReals(4, acc*c_l_charge_interval*c_l_charge_interval);
        dto.setReals(5, et);
        dto.setBooleans(0, isTerrian);
        dto.setBooleans(1, isObstacle);

        TimerStart(dto.getTimer(),c_l_charge_interval,true,function () {
            Dto dto = GetTimerData(GetExpiredTimer());
            integer faceModel = dto.getIntegers(1);
            integer count = dto.getIntegers(0);
            real step = dto.getReals(2);
            real maxStep = dto.getReals(3);
            real chargeX = dto.getReals(0);
            real chargeY = dto.getReals(1);
            real ang = GetAngleByXY(GetUnitX(dto.m_unit), GetUnitY(dto.m_unit), chargeX, chargeY);
            real dis = GetDistanceByXY(GetUnitX(dto.m_unit), GetUnitY(dto.m_unit), chargeX, chargeY);
            real et = dto.getReals(5);
            real x;
            real y;
            boolean bl = false;
            boolean isTerrian = dto.getBooleans(0);
            boolean isObstacle = dto.getBooleans(1);
            CallBack callback=0;
            
            if (count > 0) {
                count-=1;
                dto.setIntegers(0, count);
                if (step < maxStep) {
                    step += dto.getReals(4);
                    dto.setReals(2, step);
                } else {
                    dto.setReals(2, maxStep);
                }
                x = GetUnitX(dto.m_unit) + step*Cos(ang*bj_DEGTORAD);
                y = GetUnitY(dto.m_unit) + step*Sin(ang*bj_DEGTORAD);
                if (!(IsBeyondBound(x, y))) {
                    if (isObstacle) {
                        EnumDestructablesInCircle(x,y,XE_MAX_COLLISION_SIZE,function () {
                            KillDestructable(GetFilterDestructable());
                        });
                    }
                    if (isTerrian) {
                        bl=true;
                    } else {
                        if (IsTerrainWalkable(x, y)) {
                            bl=true;
                        }
                    }
                    if (bl) {
                        SetUnitX(dto.m_unit, x);
                        SetUnitY(dto.m_unit, y);
                    }
                }
                if (faceModel == 0) {
                    SetUnitFacing(dto.m_unit, ang);
                } else if (faceModel == 1) {
                    SetUnitFacing(dto.m_unit, ang + 180.0);
                }
                if (dto.m_string != null && et > 0.0) {
                    if (ModuloInteger(count, R2I(et/c_l_charge_interval))==0) {
                        DestroyEffect(AddSpecialEffectTarget(dto.m_string, dto.m_unit, "origin"));
                    }
                }
                if (dis <= maxStep) {
                    callback = dto.m_integer;
                    if (callback!=0) {
                        callback.evaluate(dto.m_data);
                    }
                    dto.destroy();
                }
            } else {
                callback = dto.m_integer;
                if (callback!=0) {
                    callback.evaluate(dto.m_data);
                }
                dto.destroy();
            }
        });
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/UnitUtils.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/ItemUtils.zinc
public struct ItemUtils[] {

    public static method getInventoryIndexByItemType(unit whichUnit, integer itemType) -> integer {
        item oItem;
        integer index;
        for (index=0; index < bj_MAX_INVENTORY; index+=1) {
            oItem = UnitItemInSlot(whichUnit, index);
            if ((oItem!=null) && (GetItemTypeId(oItem)==itemType)) {
                oItem=null;
                return index;
            }
        }
        oItem=null;
        return -1;
    }
    
    public static method delayedAddItem(unit u, integer itemType, integer slot, real timeout) {
        Dto dto = Dto.create();
        dto.m_unit = u;
        dto.m_data = itemType;
        dto.m_integer = slot;

        TimerStart(dto.getTimer(), timeout, false, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            bj_lastCreatedItem = CreateItem(dto.m_data,GetUnitX(dto.m_unit),GetUnitY(dto.m_unit));
            UnitAddItem(dto.m_unit,bj_lastCreatedItem);
            UnitDropItemSlot(dto.m_unit,bj_lastCreatedItem,dto.m_integer);
            bj_lastCreatedItem = null;
            dto.destroy();
        });
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/ItemUtils.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/SpellUtils.zinc
public struct SpellUtils[] {

	
    public static method healUnit(Unit uds, Unit uda, real healValue) {
        Unit udthreatSource;
        real hv = GetUnitState(uda.m_unit,UNIT_STATE_MAX_LIFE)-GetWidgetLife(uda.m_unit);
        if (healValue > hv) {
            healValue = hv;
        }
        SetWidgetLife(uda.m_unit,GetWidgetLife(uda.m_unit) + healValue);
        static if (LIBRARY_threat) {
            EnumUnitsInRange(GetUnitX(uds.m_unit), GetUnitY(uds.m_unit), 300, function () {
                Unit eud = Unit.get(GetEnumUnit());
                UnitThreats uthreats = UnitThreats.get(eud);

                if (uthreats > 0) {
                }
            });
        } else {
            debug BJDebugMsg("Is not import library Threat!");
        }
    }

    
    public static method boolexprEnemy(unit um, unit ua) -> boolean {
        if (IsUnitAlly(ua,GetOwningPlayer(um))) return false;
        if (!IsWidgetAlive(ua)) return false;
        //if (IsUnitType(ua,UNIT_TYPE_STRUCTURE)) return false;
        return true;
    }

    
    public static method boolexprAlly(unit um, unit ua) -> boolean {
        if (IsUnitEnemy(ua,GetOwningPlayer(um))) return false;
        if (!IsWidgetAlive(ua)) return false;
        //if (IsUnitType(ua,UNIT_TYPE_STRUCTURE)) return false;
        return true;
    }

    public static method validate(Spell spell) -> boolean {
        boolean flag = true;
        if (!IsWidgetAlive(spell.m_ud.m_unit)) {
            flag = false;
        }

        if (!flag) {
            spell.destroy();
        }
        return flag;
    }

    
    public static method getNearestUnit(Spell spell, real x, real y, real radius, integer getType) -> Unit {
        UnitGroup ugrp = UnitGroup.createEnumUnitGroup(x, y, radius);
        Unit ftUd;
        Unit ud = 0;
        real dis = radius + 1;
        real dis2;
        Link link;
        boolean flag;

        link = ugrp.getFirstLink();
        while (link > 0) {
            ftUd = Unit(link.data);
            dis2 = GetDistanceByXY(x, y, GetUnitX(ftUd.m_unit), GetUnitY(ftUd.m_unit));
            flag = false;

            if (getType == 0) {
                flag = true;
            } else if (getType == 1) {
                flag = boolexprEnemy(spell.m_ud.m_unit, ftUd.m_unit);
            } else if (getType == 2) {
                flag = boolexprAlly(spell.m_ud.m_unit, ftUd.m_unit);
            }

            if (flag && dis2 < dis) {
                ud = ftUd;
                dis= dis2;
            }

            link = link.next;
        }

        ugrp.destroy();

        return ud;
    }

    
    public static method getNearestEnemy(Spell spell, real x, real y, real radius) -> Unit {
        return getNearestUnit(spell, x, y, radius, 1);
    }

}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/SpellUtils.zinc
// BEGIN IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/SystemUtils.zinc
public struct SystemUtils[] {

	
    public static method delayRunSoundOnUnit(string file, real delay, integer timeout, unit u) {
        Dto dto = Dto.create();
        dto.m_unit = u;
        dto.m_string = file;
        dto.m_integer = timeout;

        TimerStart(dto.getTimer(), delay, false, function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            integer Sound = DefineSound(dto.m_string,dto.m_integer,false,true);
            RunSoundOnUnit(Sound,dto.m_unit);
            dto.destroy();
        });
    }
    
    public static method showSizedTexttagOnUnit(unit u, string str, player p, real size, real timeout) {
        Dto dto = Dto.create();
        dto.m_string = str;
        dto.m_texttag = CreateTextTag();
        dto.m_integer = 0;
        //隐藏漂浮文字
        SetTextTagVisibility(dto.m_texttag, false);
        //设置漂浮文字类容,大小
        SetTextTagText(dto.m_texttag, dto.m_string, (0.023/10)*size);
        //设置漂浮文字位置
        SetTextTagPos(dto.m_texttag, GetUnitX(u), GetUnitY(u), 100.00);
        //设置漂浮文字移动速度及方向
        SetTextTagVelocityBJ(dto.m_texttag, 64, 90);
        SetTextTagPermanent(dto.m_texttag, false);
        SetTextTagAge(dto.m_texttag, 0.00);
        SetTextTagFadepoint(dto.m_texttag, timeout/2.0);
        SetTextTagLifespan(dto.m_texttag, timeout);
        if (p != null) {
            //设置漂浮文字显示给哪个玩家
            SetTextTagVisibility(dto.m_texttag, GetLocalPlayer() == p);
        } else {
            //设置漂浮文字显示给所有玩家
            SetTextTagVisibility(dto.m_texttag, true);
        }
        TimerStart(dto.getTimer(),0.02,true,function() {
            Dto dto = GetTimerData(GetExpiredTimer());
            real maxSize = 15.00;
            real count = 1.00/0.02;//Time/inteval
            real cSize = -2*maxSize/(count*count)*dto.m_integer*dto.m_integer+2*maxSize/count*dto.m_integer;
            SetTextTagText(dto.m_texttag,dto.m_string,(0.023/10)*(cSize+10) );
            if (dto.m_integer<count) {
                dto.m_integer+=1;
            } else {
                dto.destroy();
            }
        });
    }
}
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/SystemUtils.zinc
    

    
    private function init() {
    }
    
    private function onInit() {
        RegisterGameStartResponse(init);
    }
}
//! endzinc
// END IMPORT OF D:\Games\War3lib\project\war3pack\src\utils/_utils.zn

////! import "pack/_pack.zn"
////! import "game/_game.zn"


//function main takes nothing returns nothing
//endfunction

