type Dto_units extends unit[32];
type Dto_effects extends effect[32];
type Dto_reals extends real[32];
type Dto_integers extends integer[32];
type Dto_strings extends string[32];
type Dto_booleans extends boolean[32];

/**
*
* Dto 数据传输对象
*
*/
public struct Dto {
    public timer m_timer = null;
    public trigger m_trigger = null;
    public player m_player = null;
    public unit m_unit = null;
    public unit m_unitB = null;
    public item m_item = null;
    public item m_itemB = null;
    public effect m_effect = null;
    public string m_string = null;
    public real m_real = 0.00;
    public real m_realB = 0.00;
    public integer m_integer = 0;
    public integer m_integerB = 0;
    public integer m_data = 0;
    public boolean m_boolean = false;
    public group m_group = null;
    public texttag m_texttag = null;
    public lightning m_lightning = null;
    public triggeraction m_action = null;
    public triggercondition m_condition = null;
    
    public Dto_effects m_effects = 0;
    public Dto_units m_units = 0;
    public Dto_reals m_reals = 0;
    public Dto_integers m_integers = 0;
    public Dto_strings m_strings = 0;
    public Dto_booleans m_booleans = 0;
    
    public static method create() -> Dto {
        Dto this = Dto.allocate();
        debug BJDebugMsg("The Number of 'Dto' is :"+I2S(this));
        return this;
    }
    
    private method hasArray() -> boolean {
        if (this.m_effects == 0 && this.m_units == 0 && this.m_reals == 0 /*
            */&& this.m_integers == 0 && this.m_strings == 0 && this.m_booleans == 0) {
            return false;
        }
        return true;
    }
    
    private method destroyArray() {
        integer i;
        for (i = 0; i < 32; i += 1) {
            this.m_effects[i] = null;
            this.m_units[i] = null;
            this.m_strings[i] = null;
        }
        this.m_effects.destroy();
        this.m_units.destroy();
        this.m_reals.destroy();
        this.m_integers.destroy();
        this.m_strings.destroy();
        this.m_effects = 0;
        this.m_units = 0;
        this.m_reals = 0;
        this.m_integers = 0;
        this.m_strings = 0;
        this.m_booleans = 0;
    }
    
    public method destroy() {
        ReleaseTimer(this.m_timer);
        ReleaseTrigger(this.m_trigger);
        ReleaseGroup(this.m_group);
        this.m_timer=null;
        this.m_effect=null;
        this.m_unit=null;
        this.m_unitB=null;
        this.m_item=null;
        this.m_itemB=null;
        this.m_trigger=null;
        this.m_group=null;
        this.m_texttag=null;
        if (this.hasArray()) {
            this.destroyArray();
        }
        this.deallocate();
    }
    
    public method delayDestroy(real timeout) {
        DelayExecuteFunc(timeout, this, function() {
            Dto this = GetDelayTimerData();
            this.destroy();
        });
    }
    
    /**
    *
    * effect数组
    *
    */
    public method getEffects(integer index) -> effect {
        return this.m_effects[index];
    }
    
    public method setEffects(integer index, effect eff) {
        if (this.m_effects == 0) {
            this.m_effects = Dto_effects.create();
        }
        this.m_effects[index] = eff;
    }
    
    /**
    *
    * unit数组
    *
    */
    public method getUnits(integer index) -> unit {
        return this.m_units[index];
    }
    
    public method setUnits(integer index, unit u) {
        if (this.m_units == 0) {
            this.m_units = Dto_units.create();
        }
        this.m_units[index] = u;
    }
    
    /**
    *
    * real数组
    *
    */
    public method getReals(integer index) -> real {
        return this.m_reals[index];
    }
    
    public method setReals(integer index, real r) {
        if (this.m_reals == 0) {
            this.m_reals = Dto_reals.create();
        }
        this.m_reals[index] = r;
    }
    
    /**
    *
    * integer数组
    *
    */
    public method getIntegers(integer index) -> integer {
        return this.m_integers[index];
    }
    
    public method setIntegers(integer index, integer i) {
        if (this.m_integers == 0) {
            this.m_integers = Dto_integers.create();
        }
        this.m_integers[index] = i;
    }
    
    /**
    *
    * 获取string数组
    *
    */
    public method getStrings(integer index) -> string {
        return this.m_strings[index];
    }
    
    public method setStrings(integer index, string str) {
        if (this.m_strings == 0) {
            this.m_strings = Dto_strings.create();
        }
        this.m_strings[index] = str;
    }
    
    /**
    *
    * 获取boolean数组
    *
    */
    public method getBooleans(integer index) -> boolean {
        return this.m_booleans[index];
    }
    
    public method setBooleans(integer index, boolean bl) {
        if (this.m_booleans == 0) {
            this.m_booleans = Dto_booleans.create();
        }
        this.m_booleans[index] = bl;
    }
    
    public method getDataTimer(integer data) -> timer {
        if (this.m_timer == null) {
            this.m_timer = NewTimer();
            SetTimerData(this.m_timer,data);
        }
        return this.m_timer;
    }

    public method getTimer() -> timer {
        return getDataTimer(this);
    }

    public method getDataTrigger(integer data) -> trigger {
        if (this.m_trigger == null) {
            this.m_trigger = NewTrigger();
            SetTriggerData(this.m_trigger,data);
        }
        return this.m_trigger;
    }

    public method getTrigger() -> trigger {
        return getDataTrigger(this);
    }
        
    public method getGroup() -> group {
        if (this.m_group == null) {
            this.m_group = NewGroup();
        }
        return this.m_group;
    }
}