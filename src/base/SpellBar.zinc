/**
*
* 施法进度条
*
*/
public struct SpellBar {
    public unit m_bar;

    public Unit m_owner;

    public CallBack m_callBack;

    private Dto m_dto;

    private static SpellBar s_data[];

    public static method get(Unit owner) -> SpellBar {
        return s_data[integer(owner)];
    }

    public method destroy() {
        RemoveUnit(this.m_bar);
        s_data[integer(this.m_owner)] = 0;
        this.m_dto.destroy();
        this.m_bar = null;
        this.m_callBack = 0;
        this.m_owner = 0;
        this.m_dto = 0;
        this.deallocate();
    }

    /**
     * 开启施法条
     * @param real     aniDurationTime 施法单位施法动作持续时间
     * @param integer  aniStartIndex   施法单位施法开始动作索引
     * @param real     aniStartSpeed   施法单位施法开始动手速度
     * @param integer  aniEndIndex     施法单位施法结束动作索引
     * @param real     aniEndSpeed     施法单位施法结束动作速度
     * @return          
     */
    public method start(real aniDurationTime, integer aniStartIndex, real aniStartSpeed, integer aniEndIndex, real aniEndSpeed) {
        Dto dto = this.m_dto;
        dto.m_unit = this.m_owner.m_unit;
        dto.m_integerB = aniEndIndex;
        dto.m_real = aniEndSpeed;
        SetUnitTimeScale(dto.m_unit, aniStartSpeed);
        if (aniStartIndex >= 0) {
            SetUnitAnimationByIndex(dto.m_unit, aniStartIndex);
        }
        if (aniDurationTime > 0) {
            TimerStart(dto.getTimer(), aniDurationTime, false, function () {
                Dto dto = GetTimerData(GetExpiredTimer());
                SetUnitAnimationByIndex(dto.m_unit, dto.m_integerB);
                SetUnitTimeScale(dto.m_unit, dto.m_real);
            });
        }
    }

    public method end() {
        SetUnitTimeScale(this.m_owner.m_unit, 1.00);
        this.destroy();
    }

    private method newBar(real duration, boolean isNormal, real height) {
        unit u = this.m_owner.m_unit;
        this.m_bar = CreateUnit(GetOwningPlayer(u), 'h004', GetUnitX(u), GetUnitY(u), 0.00);
        SetUnitFlyable(this.m_bar);
        SetUnitFlyHeight(this.m_bar, height, 0);

        if (isNormal) {
            SetUnitAnimation(this.m_bar, "attack");
        } else {
            SetUnitAnimation(this.m_bar, "spell");
        }
        SetUnitTimeScale(this.m_bar, 10.00/duration);
        this.m_dto.m_integer = R2I(duration/0.02);

        TimerStart(this.m_dto.getTimer(), 0.02, true, function () {
            Dto dto = GetTimerData(GetExpiredTimer());
            SpellBar this = SpellBar(dto.m_data);
            unit u = this.m_owner.m_unit;
            if (dto.m_integer > 0) {
                dto.m_integer -= 1;
                SetUnitX(this.m_bar, GetUnitX(u));
                SetUnitY(this.m_bar, GetUnitY(u));
            } else {
                this.m_callBack.evaluate(this.m_owner);
                this.end();
                IssueImmediateOrderById(u, ORDER_STOP);
            }
            u = null;
        });
        u = null;
    }

    /**
     * 创建施法条
     * @param Unit            owner         
     * @param real            duration          持续时间
     * @param boolean         isNormal      是否正向变满
     * @param real            height        高度
     * @param CallBack callBack             回调函数
     * @return                 
     */
    public static method create(Unit owner, real duration, boolean isNormal, real height, CallBack callBack) -> SpellBar {
        SpellBar this = SpellBar.get(owner);
        if (this > 0) {
            return this;
        }
        this = SpellBar.allocate();
        debug BJDebugMsg("create SpellBar : " + I2S(this) );
        this.m_owner = owner;
        this.m_callBack = callBack;
        this.m_dto = Dto.create();

        this.m_dto.m_data = this;
        s_data[integer(owner)] = this;

        this.newBar(duration, isNormal, height);
        return this;
    }

    public static method init() {
        trigger tr = CreateTrigger();
        TriggerRegisterAllPlayerEvent(tr, EVENT_PLAYER_UNIT_SPELL_ENDCAST, function() {
            Unit ud = Unit.get(GetTriggerUnit());
            SpellBar spellBar = SpellBar.get(ud);

            if (spellBar > 0) {
                spellBar.end();
            }
        });
        tr = null;
    }

}