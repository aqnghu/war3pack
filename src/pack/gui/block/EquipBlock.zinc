public struct EquipBlock {
	delegate Block m_block;

    public Dto m_dto;

	public Unit m_owner;

	public EquipCellArray m_cellList;

	public integer m_cellSize;

    public static CallBackList s_funcUnitAttrChange;

    private static EquipBlock s_data[];

    public static method get(Unit ud) -> EquipBlock {
        return s_data[ud];
    }

    public method clearAllEquipCell() {
        EquipCell equipCell;
        integer i;
        if (this.m_cellList > 0) {
            for (i = 0; i < this.m_cellSize; i+=1) {
                equipCell = this.m_cellList[i];
                if (equipCell > 0) {
                    equipCell.clear();
                }
            }
        }
    }

    public method clear() {
        this.m_block.clear();
        this.clearAllEquipCell();
    }

    public method removeAllEquipCell() {
        EquipCell equipCell;
        integer i;
        if (this.m_cellList > 0) {
            for (i = 0; i < this.m_cellSize; i+=1) {
                equipCell = this.m_cellList[i];
                if (equipCell > 0) {
                    equipCell.clear();
                }
                this.m_cellList[i] = 0;
            }
            this.m_cellList.destroy();
            this.m_cellList = 0;
            this.m_cellSize = 0;
        }
    }

    public method destroy() {
        this.clear();
        this.removeAllEquipCell();
        this.m_dto.destroy();
        s_data[this.m_owner] = 0;
        this.deallocate();
    }

    /**
     * 通过位置号获取
     * @param slotIndex : 位置(从0开始)
     */
    public method getEquipCellByIndex(integer slotIndex) -> EquipCell {
        EquipCell equipCell = 0;
        if (slotIndex >= 0 && slotIndex < m_cellSize) {
            equipCell = this.m_cellList[slotIndex];    
        }
        return equipCell;
    }

    private method addAllEquipCell() {
        integer rows = this.m_rowSize;
        integer cols = this.m_colSize;
        EquipCell equipCell;
        integer i, id = 0;

        for (i = 0; i< rows*cols; i+=1) {
            id = s_equipIdList[i];
            if (id != 0) {
                equipCell = EquipCell.create(this, id);
                this.m_cellList[this.m_cellSize] = equipCell;
            }
            this.m_cellSize += 1;
        }
    }

    public method drawCells() {
        integer startX = this.m_baseX;
        integer startY = this.m_baseY;
        integer rows = this.m_rowSize;
        integer cols = this.m_colSize;
        integer x = 0, y = 0, i;
        Gui ui = this.m_ui;
        Cell cell;

        EquipCell equipCell;

        for (i = 0; i< rows*cols; i+=1) {
            x = ModuloInteger(i, cols) + startX;
            y = i/cols + startY;
            cell = Cell.getByIndexXY(ui, x, y);

            equipCell = this.getEquipCellByIndex(i);

            if (equipCell > 0) {
                equipCell.setCell(cell);
                equipCell.m_indexInBlock = i;

                equipCell.drawEquip();
            }
        }
    }

    public static method create(Unit ud) -> EquipBlock {
        EquipBlock this =  EquipBlock.get(ud);
        if (this > 0) {
            return this;
        }
        this =  EquipBlock.allocate();
        debug BJDebugMsg("new EquipBlock : " +I2S(this));
        this.m_block = Block.create();
        this.m_owner = ud;
        this.m_bgTrackId = GuiGraph.s_blockBack4x5;
        this.m_cellList = EquipCellArray.create();
        this.m_cellSize = 0;
        this.m_dto = Dto.create();
        this.m_subBlock = this;

        this.setRowsCols(4, 4);
        this.addAllEquipCell();
        
        s_data[ud] = this;
        return this;
    }

    // 装备控制的人物熟悉变更
    //@      +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //@      | Bonus Type constants:       | Minimum bonus: | Maximum bonus: |
    //@      +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //@      | BONUS_LIFE
    //@      | BONUS_MANA
    //@      | BONUS_MANA_REGEN_PERCENT    |   -512%        |   +511%        |
    //@      | BONUS_LIFE_REGEN            |   -512         |   +511         |
    //@      | BONUS_ATTACK_SPEED          |   -512%        |   +511%        |
    //@      | BONUS_DAMAGE                |   -1024        |   +1023        |
    //@      | BONUS_ARMOR                 |   -1024        |   +1023        |
    //@      | BONUS_STRENGTH              |   -1024        |   +1023        |
    //@      | BONUS_AGILITY               |   -1024        |   +1023        |
    //@      | BONUS_INTELLIGENCE          |   -1024        |   +1023        |
    //@      +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public method changeUnitAttrInEquipByItem(Item itd, boolean isAdd) {
        Unit ud = this.m_owner;
        integer i;
        if (isAdd) {
            AddUnitBonus(ud.m_unit, BONUS_STRENGTH, itd.m_strength);
            AddUnitBonus(ud.m_unit, BONUS_AGILITY, itd.m_agility);
            AddUnitBonus(ud.m_unit, BONUS_INTELLIGENCE, itd.m_intelligence);
            AddUnitBonus(ud.m_unit, BONUS_DAMAGE, R2I(itd.m_attack));
            AddUnitBonus(ud.m_unit, BONUS_ARMOR, R2I(itd.m_armor));
            AddUnitBonus(ud.m_unit, BONUS_LIFE, R2I(itd.m_HP));
            AddUnitBonus(ud.m_unit, BONUS_MANA, R2I(itd.m_MP));
            AddUnitBonus(ud.m_unit, BONUS_ATTACK_SPEED, R2I(itd.m_attackSpeed));
            AddUnitBonus(ud.m_unit, BONUS_LIFE_REGEN, R2I(itd.m_regenHP));
            AddUnitBonus(ud.m_unit, BONUS_MANA_REGEN_PERCENT, R2I(itd.m_regenMP));
        } else {
            AddUnitBonus(ud.m_unit, BONUS_STRENGTH, -(itd.m_strength));
            AddUnitBonus(ud.m_unit, BONUS_AGILITY, -(itd.m_agility));
            AddUnitBonus(ud.m_unit, BONUS_INTELLIGENCE, -(itd.m_intelligence));
            AddUnitBonus(ud.m_unit, BONUS_DAMAGE, -(R2I(itd.m_attack)));
            AddUnitBonus(ud.m_unit, BONUS_ARMOR, -(R2I(itd.m_armor)));
            AddUnitBonus(ud.m_unit, BONUS_LIFE, -(R2I(itd.m_HP)));
            AddUnitBonus(ud.m_unit, BONUS_MANA, -(R2I(itd.m_MP)));
            AddUnitBonus(ud.m_unit, BONUS_ATTACK_SPEED, -(R2I(itd.m_attackSpeed)));
            AddUnitBonus(ud.m_unit, BONUS_LIFE_REGEN, -(R2I(itd.m_regenHP)));
            AddUnitBonus(ud.m_unit, BONUS_MANA_REGEN_PERCENT, -(R2I(itd.m_regenMP)));
        }

        for (i = 0; i < s_funcUnitAttrChange.size; i+=1) {
            if (s_funcUnitAttrChange[i] > 0) {
                s_funcUnitAttrChange[i].evaluate(integer(this));
            }
        }
    }

    /**
     * 是否显示使用者肖像
     */
    private method showHeroModel(boolean isShow) {
        Unit ud = this.m_owner;
        Cell cell = Cell.getByIndexXY(m_ui, this.m_baseX, this.m_baseY);
        real x = cell.m_axisX + Cell.s_width*1.8;
        real y = cell.m_axisY - Cell.s_width*2.2;
        integer modelId = UIBASE_UNITID_HERO_MODEL;

        if (isShow) {
            m_dto.m_unit = CreateUnit(GetOwningPlayer(ud.m_unit), modelId, x, y, 90.00);
            m_dto.m_effect = AddSpecialEffectTarget(ud.m_model, this.m_dto.m_unit, "origin");

            UnitUtils.setUnitFlyHeight(m_dto.m_unit, 5.00);
            UnitUtils.setUnitScale(m_dto.m_unit, 0.08);

            RefreshUnitEffectColor();
        } else {
            debug BJDebugMsg("清除单位"+GetUnitName(this.m_dto.m_unit));
            DestroyEffect(this.m_dto.m_effect);
            RemoveUnit(this.m_dto.m_unit);
            this.m_dto.m_effect = null;
            this.m_dto.m_unit = null;
        }
    }

    /**
     * 载入大背包界面
     */
    public method onLoad() {
        integer rows = this.m_rowSize;
        integer cols = this.m_colSize;
        EquipCell equipCell;
        integer i;
        
        debug BJDebugMsg("开始载入装备栏界面,x:" + I2S(this.m_baseX) +",y:" /*
        */+ I2S(this.m_baseY)+",rows:" + I2S(rows)+",cols:" + I2S(cols));
       
        // 画边框
        this.drawBorder();

        // 画背景
        this.drawBackground();

        // 画Cell
        this.drawCells();

        // 显示使用者肖像
        this.showHeroModel(true);
    }

    public method onExit() {
        this.showHeroModel(false);

        debug BJDebugMsg("Equip需要清除的Cell的数量为:" + I2S(this.m_cellSize));

        this.clear();
    }

    public static method registerUnitAttrChangeFunc(CallBack func) {
        integer i;
        for (i = 0; i < s_funcUnitAttrChange.size; i+=1) {
            if (s_funcUnitAttrChange[i] <= 0) {
                s_funcUnitAttrChange[i] = func;
            }
        }
    }

    private static IntList s_equipIdList;

    public static method init() {
        // EquipCell在BLOCK中的位置定义
        s_equipIdList = IntList.create();
        s_equipIdList[0] = EquipCell.s_headTypeId;
        s_equipIdList[3] = EquipCell.s_neckTypeId;
        s_equipIdList[4] = EquipCell.s_chestTypeId;
        s_equipIdList[7] = EquipCell.s_fingerTypeId;
        //s_equipIdList[8] = EquipCell.s_legsTypeId;
        //s_equipIdList[11] = EquipCell.s_fingerTypeId;
        s_equipIdList[8] = EquipCell.s_feetTypeId;
        s_equipIdList[11] = EquipCell.s_gloveTypeId;
        s_equipIdList[13] = EquipCell.s_mainhandTypeId;
        s_equipIdList[14] = EquipCell.s_handTypeId;

        s_funcUnitAttrChange = CallBackList.create();
    }

}