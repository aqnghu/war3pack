public struct SlotBlock {
	delegate Block m_block;

	public Unit m_owner;

	public SlotCellArray m_cellList;

	public integer m_cellSize;

    private static SlotBlock s_data[];

    public static method get(Unit ud) -> SlotBlock {
        return s_data[ud];
    }

    /**
     * 通过位置号获取背包
     * @param slotIndex : 位置(从0开始)
     */
    public method getSlotCellByIndex(integer slotIndex) -> SlotCell {
        SlotCell slotCell = this.m_cellList[slotIndex];
        return slotCell;
    }

    /**
     * 通过位置号获取小背包中物品
     * @param slotIndex : 位置(从0开始)
     */
    public method getItemByIndex(integer slotIndex) -> Item {
        item it = UnitItemInSlot(this.m_owner.m_unit, slotIndex);
        Item itd = Item.get(it);
        integer itemTypeId = GetItemTypeId(it);

        debug BJDebugMsg("SlotBlock: "+GetItemName(it) + " | " + I2S(GetItemTypeId(it)));
        debug BJDebugMsg("SlotBlock:"+ itd.m_name + " | "+I2S(itd.m_itemId)+", Idx:" + I2S(slotIndex));
        
        if (itemTypeId == 0) {
            itd = 0;
        }
        it = null;
        return itd;
    }

    /**
     * 检测背包是否已满
     */
    public method isFull() -> boolean {
        integer i;
        for (i=0; i<6; i+=1) {
            if (UnitItemInSlot(this.m_owner.m_unit, i) == null) {
                return false;
            }
        }
        return true;
    }

    public method clearAllSlotCell() {
        integer i;
        if (this.m_cellList > 0) {
            for (i = 0; i < this.m_cellSize; i+=1) {
                this.m_cellList[i].clear();
            }
        }
    }

    public method removeAllSlotCell() {
        integer i;
        if (this.m_cellList > 0) {
            for (i = 0; i < this.m_cellSize; i+=1) {
                this.m_cellList[i].clear();
                this.m_cellList[i] = 0;
            }
            this.m_cellList.destroy();
            this.m_cellList = 0;
            this.m_cellSize = 0;
        }
    }

    public method clear() {
        this.m_block.clear();
        this.clearAllSlotCell();
    }

    public method destroy() {
        this.clear();
        this.removeAllSlotCell();
        s_data[this.m_owner] = 0;
        this.deallocate();
    }

    public method addAllSlotCell() {
        integer rows = this.m_rowSize;
        integer cols = this.m_colSize;
        SlotCell slotCell;
        
        integer i;

        for (i = 0; i< rows*cols; i+=1) {
            slotCell = SlotCell.create(this);
            this.m_cellList[this.m_cellSize] = slotCell;
            this.m_cellSize += 1;
        }
    }

    public method drawCells() {
        integer startX = this.m_baseX;
        integer startY = this.m_baseY;
        integer rows = this.m_rowSize;
        integer cols = this.m_colSize;
        integer x = 0, y = 0, i;
        Cell cell;

        SlotCell slotCell;

        for (i = 0; i< rows*cols; i+=1) {
            x = ModuloInteger(i, cols) + startX;
            y = i/cols + startY;
            cell = Cell.getByIndexXY(m_ui, x, y);

            slotCell = this.getSlotCellByIndex(i);

            if (slotCell > 0) {
                slotCell.setCell(cell);
                slotCell.m_indexInBlock = i;

                slotCell.drawItem();
            }
        }
    }

    /**
     * 添加物品到背包
     * @param slotIndex : 添加位置(从0开始)
     */
    public method setItem(Item itd, integer slotIndex) {
        SlotCell slotCell = this.getSlotCellByIndex(slotIndex);

        if (!isFull()) {
            slotCell.m_indexInBlock = slotIndex;
            slotCell.setCellItem(itd);
        }
    }

    public static method create(Unit ud) -> SlotBlock {
        SlotBlock this =  SlotBlock.get(ud);
        if (this > 0) {
            return this;
        }
        this = SlotBlock.allocate();
        this.m_block = Block.create();
        this.m_owner = ud;
        this.m_cellList = SlotCellArray.create();
        this.m_cellSize = 0;
        this.m_subBlock = this;
        
        this.setRowsCols(3, 2);
        this.addAllSlotCell();

        s_data[ud] = this;
        return this;
    }

    /**
     * 载入界面
     */
    public method onLoad() {
        integer startX = this.m_baseX;
        integer startY = this.m_baseY;
        integer rows = this.m_rowSize;
        integer cols = this.m_colSize;
        SlotCell slotCell;
        Item itd;
        
        integer x = 0;
        integer y = 0;
        integer i;
        
        debug BJDebugMsg("开始载入物品槽界面,x:" + I2S(startX) +",y:" + I2S(startY)+",rows:" + I2S(rows)+",cols:" + I2S(cols));

        // 画边框
        this.drawBorder();

        // 画Cell
        this.drawCells();
    }

    public method onExit() {
        debug BJDebugMsg("Slot需要清除的Cell的数量为:" + I2S(this.m_cellSize));
        this.clear();
    }

    public static method init() {
    }

}