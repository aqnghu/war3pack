public struct GuiText {
    public Gui m_ui;

    public texttag m_string = null;
    
    public SmallDestructableList m_number;
    
    public integer m_numberSize = 0;
    
    public real m_axisX;
    public real m_axisY;
    
    public integer m_textId;

    public static integer C_STRING_ATTACK_SPEED = 1;
    public static integer C_STRING_MOVE_SPEED = 2;
    public static integer C_STRING_REGEN_HP = 3;
    public static integer C_STRING_REGEN_MP = 4;
    public static integer C_STRING_CRIT_CHANCE = 5;
    public static integer C_STRING_CRIT_VALUE = 6;
    public static integer C_STRING_MAGICRESIST_RATE = 7;
    public static integer C_STRING_HIT_CHANCE = 8;
    public static integer C_STRING_DODGE_CHANCE = 9;

    public static integer C_INTEGER_ATTACK_SPEED = 21;
    public static integer C_INTEGER_MOVE_SPEED = 22;
    public static integer C_INTEGER_REGEN_HP = 23;
    public static integer C_INTEGER_REGEN_MP = 24;
    public static integer C_INTEGER_CRIT_CHANCE = 25;
    public static integer C_INTEGER_CRIT_VALUE = 26;
    public static integer C_INTEGER_MAGICRESIST_RATE = 27;
    public static integer C_INTEGER_HIT_CHANCE = 28;
    public static integer C_INTEGER_DODGE_CHANCE = 29;

    public static integer C_STRING_DESC_TITLE = 31;
    public static integer C_STRING_DESC_CONTENT = 32;
    public static integer C_STRING_DESC_FOOTER = 33;
    
    
    private static GuiText s_data[UIBASE_MAX_PLAYERS][128];
    
    public static method get(Gui ui, integer textId) -> GuiText {
        return s_data[integer(ui)][textId];
    }

    public static method create(Gui ui, integer textId, real x, real y) -> GuiText {
        GuiText this = GuiText.get(ui, textId);
        if (this > 0) {
            return this;
        }
        this = GuiText.allocate();
        //debug BJDebugMsg("create GuiText: " +I2S(this));
        this.m_number = SmallDestructableList.create();
        this.m_axisX = x;
        this.m_axisY = y;
        this.m_ui = ui;
        this.m_textId = textId;
        s_data[integer(this.m_ui)][this.m_textId] = this;
        return this;
    }

    public method clear() {
        integer i;
        for (i = 0; i < this.m_numberSize; i += 1) {
            RemoveDestructable(this.m_number[i]);
            this.m_number[i] = null;
        }
        this.m_numberSize = 0;
        
        DestroyTextTag(this.m_string);
        this.m_string = null;
    }
    
    public method destroy() {
        clear();
        this.m_number.destroy();
        this.m_axisX = 0;
        this.m_axisY = 0;
        this.m_number = 0;
        s_data[integer(this.m_ui)][this.m_textId] = 0;
        this.deallocate();
    }

    /**
     * 添加背景
     */
    private method createBackground(integer destId, real dx, real dy) -> destructable {
        real x = this.m_axisX + dx;
        real y = this.m_axisY + dy;
        if (destId != 0) {
            return CreateDestructable(destId, x, y, 0, 1.0, 1);
        }
        return null;
    }
    /**
     * 改变背景
     * dx : 水平轴微调
     * dy ：垂直轴微调
     */
    private method changeBackground(destructable dest, integer destId, real dx, real dy) -> destructable {
        if (dest != null) {
            RemoveDestructable(dest);
        }
        return createBackground(destId, dx, dy);
    }    
    /**
     * 绘制数字
     *
     * @param value : 要绘制的数字
     * @param width : 保留的小数点后的位数
     * @param isPercent : 是否绘制百分号
     */
    public method drawNumber(real value, integer width, boolean isPercent) {
        // 按保留位数创建Integer
        Integer intd = Integer.create(R2I(Round(value*Pow(10,width))));
        real dx = Cell.s_width/5.0;
        integer i = 0;
        integer j = 0;
        integer count = 0;
        
        // 将value按每位分解并存入数组中(倒序)
        intd.toArray(1);
        
        // 处理 width > 0 并且 0<= value < 1的情况
        if (width > 0) {
            if (value == 0) {
                intd[1] = 0;
                intd[2] = 0;
            }
            if (value > 0 && value < 1) {
                intd[2] = 0;
            }
        }
        
        this.clear();

        if (isPercent) {
            this.m_number[i] = changeBackground(m_number[i],GuiGraph.s_numberText[11], dx*3 + dx/2, 0);
            i+=1;
        }
        if (width == 2) {
            this.m_number[i] = changeBackground(m_number[i],GuiGraph.s_numberText[intd[j]], dx*2+dx/2, 0);
            i+=1;
            j+=1;
            this.m_number[i] = changeBackground(m_number[i],GuiGraph.s_numberText[intd[j]], dx + dx/2, 0);
            i+=1;
            j+=1;
            this.m_number[i] = changeBackground(m_number[i],GuiGraph.s_numberText[10], dx, 0);
            i+=1;
        }
        while (j < intd.m_arrLength) {
            this.m_number[i] = changeBackground(m_number[i],GuiGraph.s_numberText[intd[j]], -dx*count, 0);
            i+=1;
            j+=1;
            count += 1;
        }
        this.m_numberSize = i;
        intd.destroy();
    }
    /**
     * 创建一个漂浮文字
     */
    public method drawString(string msg, real size) {
        if (msg == "") {
            msg = "null";
        }
        if (size == 0) {
            size = UIBASE_TEXTTAG_SIZE;
        }
        if (this.m_string == null) {
            this.m_string = CreateTextTag();
            SetTextTagPos(this.m_string, this.m_axisX, this.m_axisY, 0);
            SetTextTagColor(this.m_string, 255, 255, 255, 255);
            SetTextTagVelocity(this.m_string, 0, 0);
            SetTextTagVisibility(this.m_string, true);
            SetTextTagText(this.m_string, msg, size);
            SetTextTagPermanent(this.m_string, true);
        } else {
            SetTextTagText(this.m_string, msg, size);
        }
    }
}

public type GuiTextArray extends GuiText[32];