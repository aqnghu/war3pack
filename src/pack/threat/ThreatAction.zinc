public struct ThreatAction[] {
    private static timer s_tm;

    private static CallBackFunc s_funcs[];

    private static integer s_funcSize;

    public static method registerThreatEndResponse(CallBackFunc func) {
        s_funcs[s_funcSize] = func;
        s_funcSize += 1;
    }

    private static method filterThreatUnit(unit us, unit ua) -> boolean {
        if (IsUnitGost(ua)) {
            return false;
        }
        if (!UnitUtils.isEnemyOfNpc(us)) {
            return false;
        }
        if (UnitUtils.isEnemyOfNpc(ua)) {
            return false;
        }
        return true;
    }
    private static method filterAttackUnit(unit us,unit ua) -> boolean {
        if (!IsWidgetAlive(ua)) {
            return false;
        }
        if (!IsUnitInRange(ua,us,800.00)) {
            return false;
        }
        if (IsUnitGost(ua)) {
            return false;
        }
        if (IsUnitType(ua,UNIT_TYPE_FLYING) && (!IsUnitType(us,UNIT_TYPE_ATTACKS_FLYING))) {
            return false;
        }
        return true;
    }

    public static method flush() {
        UnitThreats uThreats;
        integer i;

        uThreats = UnitThreats.last;
        while (uThreats > 0) {
            ThreatTransmit.removeByUnitThreats(uThreats);
            uThreats.destroy();
            uThreats = uThreats.prev;
        }
    }

    public static method end(real timeout) {
        TimerStart(s_tm, timeout, false, function () {
            integer i;
            for (i = 0; i < s_funcSize; i += 1) {
                s_funcs[i].evaluate();
            }
            flush();
            debug BJDebugMsg("ThreatAction end!");
        });
    }

    private static method getUnitThreats(Unit ud) -> UnitThreats {
        UnitThreats uThreats = UnitThreats.get(ud);
        if (uThreats > 0) {
            return uThreats;
        }

        uThreats = UnitThreats.create(ud);
        TimerStart(uThreats.getTimer(), 0.30, true, function () {
            UnitThreats uThreats = GetTimerData(GetExpiredTimer());
            Threat threat;
            integer i;
            unit ua, ub;

            uThreats.sortByThreatValue();

            if (uThreats.m_isAttackTarget && /*
            */ ModuloInteger(uThreats.getNextExpiredTimerCount(), R2I(uThreats.m_attackOrderInterval/0.30)) == 0) {
                
                for (i = 0; i < uThreats.m_threatsLength; i+=1) {
                    threat = uThreats.m_threats[i];
                    ua = threat.m_unitSource.m_unit;
                    ub = threat.m_unitTarget.m_unit;

                    if (filterAttackUnit(ua, ub)) {
                        uThreats.m_attackTarget = threat.m_unitTarget;
                        IssueTargetOrderById(ua, ORDER_ATTACK, ub);
                        break;
                    }
                }
            }
            ua = null;
            ub = null;
        });
        return uThreats;
    }

    public static method start(unit damagedUnit, unit damageSource, real damage) {
        Unit udSource, udTarget;
        Threat threat;
        UnitThreats uThreats;
        ThreatTransmit trans;

        if (filterThreatUnit(damagedUnit, damageSource)) {
            udSource = Unit.get(damagedUnit);
            udTarget = Unit.get(damageSource);
            uThreats = getUnitThreats(udSource);
            threat = uThreats.getThreatByUnitTarget(udTarget);

            if (threat <= 0) {
                threat = Threat.create(udSource,udTarget);
                uThreats.addThreat(threat);
            }

            trans = ThreatTransmit.get(threat);
            if (trans <= 0) {
                threat.addThreatValue(damage);
            } else {
                trans.shareValue(damage);
            }
            //定时结束战斗
            end(15.00);
        }
    }

    private static method threatEndWithUnitDeath(unit u) {
        Unit ud = Unit.get(u);
        UnitThreats unitThreats = 0;
        Threat threat;
        integer i;

        unitThreats = UnitThreats.get(ud);
        if (unitThreats > 0) {
            ThreatTransmit.removeByUnitThreats(unitThreats);
            unitThreats.destroy();
            return;
        }

        unitThreats = UnitThreats.last;
        while (unitThreats > 0) {
            threat = unitThreats.getThreatByUnitTarget(ud);
            ThreatTransmit.removeByThreat(threat);
            unitThreats.removeThreat(threat);
            unitThreats = unitThreats.prev;
        }
    }

    private static method refreshBoard(integer data) {
        Playor pd = Playor(data);
        BoardWrapper board = BoardWrapper.get(pd);
        ThreatView view = ThreatView.get(board);
        Unit ud = view.m_udTarget;
        
        if (view.m_isShow) {
            if (!board.m_isClearing) {
                view.refreshBoard();
            }
        }
        if (ud != 0 && !IsUnitAlive(ud.m_unit)) {
            view.release();
        }
    }
    
    private static method zeroStart() {
        trigger tr = CreateTrigger();
        Playor pd;
        integer i;
        for (i = 0; i < bj_MAX_PLAYERS; i += 1) {
            pd = Playor.get(Player(i));
            if (IsPlayerControllerIsUser(pd.m_player)) {
                ThreatView.create(BoardWrapper.get(pd));

                TriggerRegisterPlayerUnitEvent(tr, pd.m_player, EVENT_PLAYER_UNIT_ISSUED_TARGET_ORDER, null);
                TriggerRegisterPlayerUnitEvent(tr, pd.m_player, EVENT_PLAYER_UNIT_ISSUED_ORDER, null);
            }
        }

        TriggerAddCondition(tr, Condition(function () {
            Unit udSource = Unit.get(GetOrderedUnit());
            Unit udTarget;
            Playor pd = Playor.get(GetOwningPlayer(udSource.m_unit));
            ThreatView view = ThreatView.get(BoardWrapper.get(pd));
            string effectStr;
            //右键点击
            if (GetIssuedOrderId() == ORDER_RIGHT_CLICK) {
                udTarget = Unit.get(GetOrderTargetUnit());
                if (UnitUtils.isEnemyOfNpc(udTarget.m_unit) && udTarget != view.m_udTarget) {
                    if (GetLocalPlayer() == pd.m_player) {
                        effectStr = "DrEffect\\SelectAura.mdl";
                    } else {
                        effectStr = "";
                    }
                    view.release();
                    view.setTarget(udTarget);
                    view.m_targetEffect = AddSpecialEffectTarget(effectStr, udTarget.m_unit, "origin");
                }
            //保持警戒位置
            } else if (GetIssuedOrderId() == ORDER_GUARD) {
                view.release();
            }
            effectStr = null;
        }));
        tr = null;
    }

    public static method init() {
        s_funcSize = 0;
        s_tm = CreateTimer();

        RegisterUnitDeathResponse(ThreatAction.threatEndWithUnitDeath);
        RegisterDamageResponse(ThreatAction.start);

        RegisterGameStartResponse(ThreatAction.zeroStart);
        RegisterPlayerPeriodicResponse(ThreatAction.refreshBoard);
        //RegisterThreatEndResponse(clearBoard);
    }
}