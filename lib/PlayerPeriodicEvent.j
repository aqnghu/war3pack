library PlayerPeriodicEvent initializer onInit requires TimerUtils,GameStartEvent

    public function interface Response takes integer playerId returns nothing

    globals
        private Response array callBacks
        private integer index = 0
    endglobals

    // ============================[API]====================================
    function RegisterPlayerPeriodicResponse takes Response r returns nothing
        set callBacks[index] = r
        set index = index + 1
    endfunction
    // ================================================================
    private function eventFunc takes nothing returns nothing
        local integer pd = GetTimerData(GetExpiredTimer())
        local integer i = 0
        loop
            exitwhen i >= index
            if callBacks[i] > 0 then
                call callBacks[i].evaluate(pd)
            endif
            set i = i + 1
        endloop
    endfunction
    private function init takes nothing returns nothing
        local integer i = 0
        local timer tm
        loop
            exitwhen i >= bj_MAX_PLAYERS
            set tm = NewTimer()
            call SetTimerData(tm, i)
            call TimerStart(tm, 0.10, true, function eventFunc)
            set i = i + 1
        endloop
        set tm = null
    endfunction
    private function onInit takes nothing returns nothing
        call RegisterGameStartResponse(init)
    endfunction

endlibrary
