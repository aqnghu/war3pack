// DataCache for 1.24+
//
library DataCache initializer init requires ListModule

struct DataCache
    public hashtable m_ht = null

    public string m_key

    implement StructList

    static method get takes string pkey returns DataCache
        local DataCache d = DataCache.first
        loop
            exitwhen d <= 0
            if d.m_key == pkey then
                return d
            endif
            set d = d.next
        endloop
        return 0
    endmethod

    static method create takes string pkey returns DataCache
        local DataCache this =  DataCache.get(pkey)
        if (this > 0) then
            return this
        endif
        set this = DataCache.allocate()
        set this.m_ht = InitHashtable()
        call this.listAdd()
        return this
    endmethod

    method destroy takes nothing returns nothing
        call this.listRemove()
        call this.deallocate()
    endmethod

    // integer
    method saveInteger takes integer parentKey, integer childKey, integer value returns nothing
        call SaveInteger(this.m_ht, parentKey, childKey, value)
    endmethod

    method loadInteger takes integer parentKey, integer childKey returns integer
        return LoadInteger(this.m_ht, parentKey, childKey)
    endmethod

    method haveSavedInteger takes integer parentKey, integer childKey returns boolean
        return HaveSavedInteger(this.m_ht, parentKey, childKey)
    endmethod

    method removeSavedInteger takes integer parentKey, integer childKey returns nothing
        call RemoveSavedInteger(this.m_ht, parentKey, childKey)
    endmethod

    // real
    method saveReal takes integer parentKey, integer childKey, real value returns nothing
        call SaveReal(this.m_ht, parentKey, childKey, value)
    endmethod

    method loadReal takes integer parentKey, integer childKey returns real
        return LoadReal(this.m_ht, parentKey, childKey)
    endmethod

    method haveSavedReal takes integer parentKey, integer childKey returns boolean
        return HaveSavedReal(this.m_ht, parentKey, childKey)
    endmethod

    method removeSavedReal takes integer parentKey, integer childKey returns nothing
        call RemoveSavedReal(this.m_ht, parentKey, childKey)
    endmethod

    // boolexpr
    method saveBooleanExprHandle takes integer parentKey, integer childKey, boolexpr value returns nothing
        call SaveBooleanExprHandle(this.m_ht, parentKey, childKey, value)
    endmethod

    method loadBooleanExprHandle takes integer parentKey, integer childKey returns boolexpr
        return LoadBooleanExprHandle(this.m_ht, parentKey, childKey)
    endmethod

    // region
    method saveRegionHandle takes integer parentKey, integer childKey, region value returns nothing
        call SaveRegionHandle(this.m_ht, parentKey, childKey, value)
    endmethod

    method loadRegionHandle takes integer parentKey, integer childKey returns region
        return LoadRegionHandle(this.m_ht, parentKey, childKey)
    endmethod

    // rect
    method saveRectHandle takes integer parentKey, integer childKey, rect value returns nothing
        call SaveRectHandle(this.m_ht, parentKey, childKey, value)
    endmethod

    method loadRectHandle takes integer parentKey, integer childKey returns rect
        return LoadRectHandle(this.m_ht, parentKey, childKey)
    endmethod

    // sound
    method saveSoundHandle takes integer parentKey, integer childKey, sound value returns nothing
        call SaveSoundHandle(this.m_ht, parentKey, childKey, value)
    endmethod

    method loadSoundHandle takes integer parentKey, integer childKey returns sound
        return LoadSoundHandle(this.m_ht, parentKey, childKey)
    endmethod

    // common
    method haveSavedHandle takes integer parentKey, integer childKey returns boolean
        return HaveSavedHandle(this.m_ht, parentKey, childKey)
    endmethod

    method removeSavedHandle takes integer parentKey, integer childKey returns nothing
        call RemoveSavedHandle(this.m_ht, parentKey, childKey)
    endmethod

    method flush takes integer parentKey returns nothing
        call FlushChildHashtable(this.m_ht, parentKey)
    endmethod

endstruct

function init takes nothing returns nothing

endfunction

endlibrary
