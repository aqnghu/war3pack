library GetUnitCollisionSize
//********************************************************
//* GetUnitCollisionSize (Standalone version)
//* --------------------
//*   If you need it, use it.
//*
//* To implement it just create a custom text 'trigger'
//* called GetUnitCollisionSize, and paste this there.
//*
//* To copy from one map to another just copy the trigger
//* holding this code to the target map.
//*
//*********************************************************

//========================================================
    globals
        private constant integer ITERATIONS         = 10    //too much, slow, too short "innacurate", let it be bigger than 0...
                                                            //I *think* 10 is enough...
        public  constant real    MAX_COLLISION_SIZE = 300.0 //should be THE max collision size in the map,
                                                            //well, not really, just make sure it is greater than it
                                                            //few maps should have collision sizes bigger than 300.0...
    endglobals

//========================================================
function GetUnitCollisionSize takes unit u returns real
 local integer i=0
 local real x=GetUnitX(u)
 local real y=GetUnitY(u)
 local real hi
 local real lo
 local real mid


    set hi=MAX_COLLISION_SIZE
    set lo=0.0
    loop
        set mid=(lo+hi)/2.0
        exitwhen (i==ITERATIONS)
        if (IsUnitInRangeXY(u,x+mid,y,0)) then
            set lo=mid
        else
            set hi=mid
        endif
        set i=i+1
    endloop
 return mid
endfunction

endlibrary
