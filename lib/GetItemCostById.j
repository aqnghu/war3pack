library GetItemCostById initializer Init requires Table,xebasic
//===========================================================================
//  vJASS version of weaaddar's GetItemCostById system
//===========================================================================
//
//  * Uses Table system
//  * Fixed an issue that could happen if item cost was zero for both gold and lumber
//  * This version is compatible with original one
//  * Added some helper functions: GetItemGoldCost, GetItemLumberCost, GetItemGoldCostById, GetItemLumberCostById
//
//===========================================================================
globals
    private constant player  BuyerPlayer = Player(15)
    private constant integer BuyerTypeId = XE_DUMMY_UNITID
    private constant integer ShopTypeId = XE_DUMMY_UNITID
    private constant integer MAX_PRICE = 50000

    // check Init function
    private Table GoldPrice = 0
    private Table LumberPrice = 0
    private unit Buyer = null
    private unit Shop = null
endglobals

//===========================================================================
//  whichtype == true --> gold
//  whichtype == false --> lumber
//===========================================================================
function GetItemCostById takes integer id, boolean whichtype returns integer
    local integer gold
    local integer lumber

    if GoldPrice.exists(id) then
        set gold = GoldPrice[id]
        set lumber = LumberPrice[id]
    else
        call SetPlayerState(BuyerPlayer, PLAYER_STATE_RESOURCE_GOLD,   MAX_PRICE)
        call SetPlayerState(BuyerPlayer, PLAYER_STATE_RESOURCE_LUMBER, MAX_PRICE)
        call AddItemToStock(Shop, id, 1, 1)
        call IssueNeutralImmediateOrderById(BuyerPlayer, Shop, id)
        call RemoveItem(UnitItemInSlot(Buyer, 0))
        call RemoveItemFromStock(Shop, id)
        set gold   = MAX_PRICE - GetPlayerState(BuyerPlayer, PLAYER_STATE_RESOURCE_GOLD)
        set lumber = MAX_PRICE - GetPlayerState(BuyerPlayer, PLAYER_STATE_RESOURCE_LUMBER)
        set GoldPrice[id] = gold
        set LumberPrice[id] = lumber
    endif

    if whichtype then
        return gold
    else
        return lumber
    endif
endfunction

//===========================================================================
function GetItemGoldCost takes item i returns integer
    return GetItemCostById(GetItemTypeId(i), true)
endfunction

//===========================================================================
function GetItemLumberCost takes item i returns integer
    return GetItemCostById(GetItemTypeId(i), false)
endfunction

//===========================================================================
function GetItemGoldCostById takes integer id returns integer
    return GetItemCostById(id, true)
endfunction

//===========================================================================
function GetItemLumberCostById takes integer id returns integer
    return GetItemCostById(id, false)
endfunction

//===========================================================================
private function Init takes nothing returns nothing
    local rect r = GetWorldBounds()
    local real x = GetRectMaxX(r)-100
    local real y = GetRectMaxY(r)-100

    set Buyer = CreateUnit(BuyerPlayer, BuyerTypeId, 0, 0, 0)
    call SetUnitX(Buyer, x)
    call SetUnitY(Buyer, y)
    debug call BJDebugMsg("Buyer"+GetUnitName(Buyer)+"has been create")
    set Shop = CreateUnit(BuyerPlayer, ShopTypeId, 0, 0, 0)
    call SetUnitX(Shop,x)
    call SetUnitY(Shop,y)
    debug call BJDebugMsg("Shop"+GetUnitName(Shop)+"has been create")
    call UnitAddAbility(Shop, 'Asid')
    call UnitRemoveAbility(Shop, 'Awan')
    call UnitAddAbility(Shop, 'Aloc')

    set GoldPrice = Table.create()
    set LumberPrice = Table.create()

    call RemoveRect(r)
    set r = null
endfunction

endlibrary

