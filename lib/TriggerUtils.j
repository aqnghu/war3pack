library TriggerUtils initializer onInit requires Table
    globals
        private Table tab
    endglobals

    function NewTrigger takes nothing returns trigger
        return CreateTrigger()
    endfunction

    function SetTriggerData takes trigger tr, integer value returns trigger
        set tab[GetHandleId(tr)] = value
        return tr
    endfunction

    function GetTriggerData takes trigger tr returns integer
        return tab[GetHandleId(tr)]
    endfunction

    function ReleaseTrigger takes trigger tr returns nothing
        if tr == null then
            return
        endif
        call tab.flush(GetHandleId(tr))
        call DestroyTrigger(tr)
    endfunction

    function GetTriggeringTriggerData takes nothing returns integer
        local trigger tr = GetTriggeringTrigger()
        local integer data = GetTriggerData(tr)
        call ReleaseTrigger(tr)
        set tr = null
        return data
    endfunction

    function TriggerRegisterAllPlayerEvent takes trigger tr, playerunitevent whichevent, code condition returns nothing
        local integer i
        set i = 0
        loop
            exitwhen i >= bj_MAX_PLAYER_SLOTS
            call TriggerRegisterPlayerUnitEvent(tr, Player(i), whichevent, null)
            set i = i + 1
        endloop
        call TriggerAddCondition(tr, Condition(condition))
    endfunction

    private function onInit takes nothing returns nothing
        set tab = Table.create()
    endfunction
endlibrary
