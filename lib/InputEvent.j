library InputEvent initializer onInit

    globals
        private Table tab
    endglobals

    public function interface Response takes player p, string msg returns nothing

    // ================================================================
    /**
     * API
     */
    function RegisterInputResponse takes string str, Response r returns nothing
        if str!="" then
            set tab[StringHash(str)] = integer(r)
        endif
    endfunction

    // ================================================================

    private function InputCalls takes nothing returns nothing
        local integer hash = StringHash(GetEventPlayerChatString())
        if tab.exists(hash) then
            call Response(tab[hash]).evaluate(GetTriggerPlayer(), GetEventPlayerChatString())
        endif
    endfunction

    private function ClearScreen takes player p, string msg returns nothing
        if GetLocalPlayer() == p then
            call ClearTextMessages()
        endif
    endfunction

    private function InitTrigger takes code c returns nothing
        local trigger tr = CreateTrigger()
        local integer i
        call TriggerAddCondition(tr, Condition(c))

        set i = 0
        loop
            exitwhen i >= bj_MAX_PLAYERS
            call TriggerRegisterPlayerChatEvent(tr, Player(i), "-", false)
            set i = i + 1
        endloop

        set tr=null
    endfunction

    private function onInit takes nothing returns nothing
        set tab=Table.create()
        call InitTrigger(function InputCalls)
        call RegisterInputResponse("-clear", ClearScreen)
    endfunction

endlibrary
