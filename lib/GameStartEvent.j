library GameStartEvent initializer onInit

    public function interface Response takes nothing returns nothing

    globals
        private Response array callBacks
        private integer index = 0
    endglobals

    // ============================[API]====================================
    function RegisterGameStartResponse takes Response r returns nothing
        set callBacks[index] = r
        set index = index + 1
    endfunction
    // ================================================================

    private function eventFunc takes nothing returns nothing
        local integer i
        set i = 0
        loop
            exitwhen i >= index
            call callBacks[i].evaluate()
            set i = i + 1
        endloop
        call DestroyTrigger(GetTriggeringTrigger())
    endfunction
    private function onInit takes nothing returns nothing
        local trigger tr = CreateTrigger()
        call TriggerRegisterTimerEvent(tr, 0.00, false)
        call TriggerAddCondition(tr, Condition(function eventFunc))
        set tr = null
    endfunction

endlibrary
