library UnitDeathEvent initializer onInit requires TriggerUtils

    public function interface Response takes unit u returns nothing

    globals
        private Response array callBacks
        private integer index = 0
    endglobals

    // ============================[API]====================================
    function RegisterUnitDeathResponse takes Response r returns nothing
        set callBacks[index] = r
        set index = index + 1
    endfunction
    // ================================================================

    private function eventFunc takes nothing returns nothing
        local integer i
        set i = 0
        loop
            exitwhen i >= index
            call callBacks[i].evaluate(GetTriggerUnit())
            set i = i + 1
        endloop
    endfunction
    private function onInit takes nothing returns nothing
        local trigger tr = CreateTrigger()
        call TriggerRegisterAllPlayerEvent(tr, EVENT_PLAYER_UNIT_DEATH, function eventFunc)
        set tr = null
    endfunction

endlibrary
