library WorldUtils initializer onInit

    globals
        public rect World_worldRect=null
        public region World_worldRegion=null
        public location World_initLoc=null
        public real World_maxX=0.0
        public real World_maxY=0.0
        public real World_minX=0.0
        public real World_minY=0.0
        public real World_centerX=0.0
        public real World_centerY=0.0
    endglobals

    function IsBeyondBound takes real x, real y returns boolean
        return (x > World_maxX) or (x < World_minX) or (y > World_maxY) or (y < World_minY)
    endfunction

    function IsWidgetInWorld takes widget w returns boolean
        local real maxx = GetRectMaxX(bj_mapInitialPlayableArea) - 256.0
        local real maxy = GetRectMaxY(bj_mapInitialPlayableArea) - 256.0
        local real minx = GetRectMinX(bj_mapInitialPlayableArea) + 256.0
        local real miny = GetRectMinY(bj_mapInitialPlayableArea) + 256.0
        local real x = GetWidgetX(w)
        local real y = GetWidgetY(w)
        return (minx <= x) and (x <= maxx) and (miny <= y) and (y <= maxy)
    endfunction

    private function onInit takes nothing returns nothing
        set World_worldRect = GetWorldBounds()
        set World_worldRegion = CreateRegion()
        set World_maxX = GetRectMaxX(World_worldRect)
        set World_maxY = GetRectMaxY(World_worldRect)
        set World_minX = GetRectMinX(World_worldRect)
        set World_minY = GetRectMinY(World_worldRect)
        set World_initLoc = Location(World_minX,World_maxY)
        set World_centerX = (World_maxX + World_minX) / 2
        set World_centerY = (World_minY + World_maxY) / 2
        call RegionAddRect(World_worldRegion, World_worldRect)
    endfunction

endlibrary
