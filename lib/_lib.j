//-------------Base
//! import "../lib/DataCache.j"
//! import "../lib/Table.j"
//! import "../lib/TimerUtils.j"
//! import "../lib/TriggerUtils.j"
//! import "../lib/AutoIndex.j"
//! import "../lib/Itemdex.j"
//! import "../lib/ListModule.j"
//! import "../lib/LinkedList.j"
//! import "../lib/CycList.j"
//! import "../lib/Stack.j"
//! import "../lib/SortUtils.j"
//! import "../lib/GroupUtils.j"
//! import "../lib/SoundUtils.j"
//! import "../lib/ARGB.j"
//! import "../lib/GetProc.j"
////! import "../lib/ImageUtils.j"
//! import "../lib/RectUtils.j"
//! import "../lib/WorldUtils.j"
//! import "../lib/MoveSpeedXGUI.j"
//! import "../lib/LastOrder.j"
//---------------Preload
//! import "../lib/AbilityPreload.j"
//---------------Event
//! import "../lib/SpellEvent.j"
//! import "../lib/DamageEvent.j"
//! import "../lib/ItemEvent.j"
//! import "../lib/GameStartEvent.j"
//! import "../lib/InputEvent.j"
//! import "../lib/UnitDeathEvent.j"
//! import "../lib/PlayerPeriodicEvent.j"
//---------------Math
//! import "../lib/Vector.j"
//! import "../lib/Logarithm.j"
//----------------Gui
//! import "../lib/SimError.j"
//! import "../lib/Dialog.j"
//! import "../lib/Board.j"
//! import "../lib/Multibars.j"
//! import "../lib/GameClock.j"
//----------------Toolkit
//! import "../lib/DelFX.j"
//! import "../lib/UnitFadeSystem.j"
//! import "../lib/OnDoubleClick.j"
//! import "../lib/TerrainPathability.j"
//! import "../lib/LineSegment.j"
//! import "../lib/IsPointInPoly.j"
//! import "../lib/GetNearest.j"
//! import "../lib/GetItemCostById.j"
//! import "../lib/UnitStatus.j"
//! import "../lib/GetUnitCollisionSize.j"
//! import "../lib/ExtBJ.j"
//-------------------Bonus
//! import "../lib/BonusMod.j"
//! import "../lib/UnitMaxStateBonuses.j"
//! import "../lib/UnitMaxState.j"
//! import "../lib/ArmorUtils.j"
//-------------------xe
//! import "../lib/xebasic.j"


library lib requires Table,TimerUtils,TriggerUtils,AutoIndex,Itemdex,ListModule/*
*/,LinkedList,CycList,Stack,SortUtils,GroupUtils,SoundUtils/*
*/,ARGB,GetProc,RectUtils,WorldUtils,MoveSpeedXGUI,LastOrder,AbilityPreload,SpellEvent/*
*/,DamageEvent,ItemEvent,GameStartEvent,InputEvent,UnitDeathEvent/*
*/,vector,Logarithm,SimError,Dialog,Board,Multibars/*
*/,GameClock,DelFX,UnitFadeSystem,OnDoubleClick,TerrainPathability/*
*/,LineSegment,IsPointInPoly,GetNearest,GetItemCostById,UnitStatus/*
*/,GetUnitCollisionSize,ExtBJ/*
*/,BonusMod,UnitMaxStateBonuses,UnitMaxState,ArmorUtils,xebasic
    
endlibrary
