library ItemEvent initializer onInit
// ================================================================
    
    public function interface Response takes unit u, item it returns nothing
    
// ================================================================
    
    globals
        private Response array responses
        private integer responsesCount = 0
        
        private Response array responsesB
        private integer responsesCountB = 0
    endglobals

    private function pickup takes nothing returns nothing 
        local integer i = 0
        loop
            exitwhen i>=responsesCount
            call responses[i].evaluate(GetTriggerUnit(), GetManipulatedItem())
            set i=i+1
        endloop
    endfunction
    
    private function drop takes nothing returns nothing
        local integer i = 0
        
        if (GetWidgetLife(GetManipulatedItem()) <= 0) then
            call RemoveItem(GetManipulatedItem())
            return
        endif
        
        loop
            exitwhen i>=responsesCountB
            call responsesB[i].evaluate(GetTriggerUnit(), GetManipulatedItem())
            set i=i+1
        endloop
    endfunction
    
    function RegisterPickupItemResponse takes Response r returns nothing
        set responses[responsesCount] = r
        set responsesCount=responsesCount+1
    endfunction
    
    function RegisterDropItemResponse takes Response r returns nothing
        set responsesB[responsesCountB] = r
        set responsesCountB=responsesCountB+1
    endfunction

// ================================================================

    private function InitTrigger takes playerunitevent e, code c returns nothing
        local trigger t=CreateTrigger()
        call TriggerRegisterAnyUnitEventBJ( t, e )
        call TriggerAddCondition(t, Condition(c))
        set t=null
    endfunction
    private function onInit takes nothing returns nothing
        call InitTrigger(EVENT_PLAYER_UNIT_PICKUP_ITEM, function pickup)
        call InitTrigger(EVENT_PLAYER_UNIT_DROP_ITEM, function drop)
    endfunction

endlibrary